Dome Tools
-------------------

The purpose of developing integrated simulations is to allow a wide variety of individuals, ranging from scientists to designers and policy-makers, to explore the characteristics of different alternatives. Tools are special models that run other models for different analysis purposes.  There are a number of different analysis tools that have been wrapped as plugin models so that they can interface with DOME models.
