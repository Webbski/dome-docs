Server Mode
-------------------

The Server Mode application is an administrative tool. It is used to determine how models and playspaces are organized, track use, perform software maintenance, and define users and groups. 