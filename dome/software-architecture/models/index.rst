Models
=======================


Models are DOME objects that provide executable simulation capabilities. Fundamentally, there are three different kinds of models: DOME-native; plugins; and integration projects. From a use standpoint they look the same, but they differ in how their internal computation is performed. DOME-native models are completely implemented in the DOME environment and during execution all computation is performed within DOME. DOME models are most commonly used within integration projects to define relationships between other model interfaces. Plugin models are models where the underlying computation is performed by an external software application, such as a spreadsheet, CAD system, commercial simulation tools, analysis tools, optimization tools, or models written in a programming language. Plugin models wrap the non-native computation with a DOME model and interfaces. An integration project is a special form of model that is used to connect or integrate other models. Integration projects have standard model interfaces

.. toctree::
   :titlesonly:

   dome-native-models.rst
   plugin-models.rst
