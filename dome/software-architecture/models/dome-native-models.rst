Dome Native Models
-------------------

DOME-native models are completely implemented in the DOME environment and during execution all of the simulation computation is performed within DOME. Simulations are created by defining DOME parameters within the model. These parameters can then be linked using mathematical DOME relationships. When changes to the value of parameters are made, the DOME server in which the model resides will solve the model's relationship network and determine which relationships need to be executed and in what order. 