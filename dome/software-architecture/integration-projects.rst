Integration Projects
---------------------

Integration Projects are a special type of model. They may contain other models or projects as computation resources, along with integration models that define interactions between resources. Integration models are similar to DOME models, with the exception that they may utilize parameters from the project resources and they may not be used independent of the project. Thus, projects are used to build integrated simulations. Since projects are a type of model they may have interfaces and user defined documentation. 