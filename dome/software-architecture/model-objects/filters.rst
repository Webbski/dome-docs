Filters
-------------------

Filter objects are a special type of context. Filters automatically generate views containing entities with specific common characteristics. For example, a relations filter applied to the scope of a complete model will automatically generate a visualization of all relationship objects in the model. Similarly, an independent parameter filter will automatically generate a view showing all parameters that are not driven by relationships.  Filters are used as tool to create visual structures that aid in the understanding of DOME models (standard views, for example). 