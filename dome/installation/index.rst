DOME Installation
====================

.. warning:: These instructions, intended for developers, point to the latest working version of DOME. Users interested in the latest stable version can find instructions `on the wiki <https://digitalmfgcommons.atlassian.net/wiki/display/DMDIIDMC/Installing+and+Launching+DOME>`_.

This page contains basic instructions for installing DOME. For more details, please see the subpages.

Topics
------

.. toctree::
    :titlesonly:

    dev-environment-setup.rst
    howto-checkout-run.rst

Quick Start
-----------

Clone `the DOME repository <https://bitbucket.org/DigitalMfgCommons/dome>`_ using command-line git or your favorite GUI:
::

  sudo apt-get -y install git
  cd ~
  git clone https://<username>@bitbucket.org/DigitalMfgCommons/dome.git

Install ant
::

  # check whether ant is already installed on your system (likely)
  ant -version

  # install ant if necessary
  sudo apt-get install ant

Build DOME
::

  cd digital-manufacturing-commons/DOME   # Navigate to the directory containing build.xml
  ant dist-client                         # build client
  ant dist-server                         # build server
  
  # Other options:
  ant dist-war                            # build DOME war file
  ant clean                               # remove artifacts created by ant

