Development Environment Setup
===============================

All three repositories are hosted using git.  You can clone the repository using your favorite git tool.  Below is a description on how to clone the repository using the git within Eclipse.

To configure GIT to connect to the DMC repository, follow these directions: Setup Proxy for GIT Repo

GIT Repositories
------------------
The code repository for:

 * VehicleForge is located at https://bitbucket.org/DigitalMfgCommons/vehicleforge
 * DOME is located at https://bitbucket.org/DigitalMfgCommons/dome
 
Configure Git Repositories in Eclipse
---------------------------------------

Step 1: Navigate to "Git Repository Explorer" perspective and select "Clone a Git Repository"

.. image:: _static/dev-setup/step1-connect-to-git.png

Step 2: Fill in repository details

 * Host: bitbucket.org
 * Protocol: https
 
+--------------+-------------------------------------+
|              | Repository Path                     |
+==============+=====================================+
| VehicleForge | /DigitalMfgCommons/vehicleforge.git |
+--------------+-------------------------------------+
| DOME         | /DigitalMfgCommons/dome.git         |
+--------------+-------------------------------------+

Then enter your username and password and press "Next"

.. image:: _static/dev-setup/step2-setup-git.png

Step 3: Select the master branch to clone

.. image:: _static/dev-setup/step3-select-master.png

Step 4: (1) Choose the local destination and then (2) click "Finish"

The git repository will now be cloned.

.. image:: _static/dev-setup/step4-finish-clone.png
