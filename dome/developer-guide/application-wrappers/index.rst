Application Wrappers
=====================

The Plug-in API is used to create wrappers which allow simulations in external applications to interface with other DOME models. Then, any external simulation using the given external application can be seamlessly integrated with all other DOME models. Several software programs are supported by DOME3 plug-ins, but the API allows programmers to interface with applications that are not supported by the released software.


.. toctree::
   :titlesonly:

   batch-plugins.rst
   api-plugins.rst
