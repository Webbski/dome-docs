Extending DOME3
=================

This section will provide documentation for writing programs that extend core DOME functionality. For example, it could be used to write new data types for DOME parameters or to write a program that allows DOME itself to operate as a plugin to other applications. 
The documentation will become available in the fall of 2002.