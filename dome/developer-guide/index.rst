Developer Guide 
=================

The developer guide provides API documentation to be used by software programmers developing new applications intended to interact with DOME.

At present the DOME APIs are under refinement and testing. API documentation will be rolled out over the summer of 2002.  The Wrapping Applications (Plugin API) documentation will be released first since this API is already in use for all of the 3rd Party Application Models currently available in DOME3.

The API for writing applications that extend the core functionality of DOME will be released second. Finally, there will be a GUI API that will allow the creation custom interfaces for DOME objects. The GUI API is last because the GUI Configurator tool already allows for the creation of custom GUIs that can be connected to the services of DOME models.

.. toctree::
   :titlesonly:

   application-wrappers/index
   extending-dome.rst
   dome-javadoc.rst
