Running Models as a User 
========================

In this tutorial you will log in to a DOME server as a user, navigate the server file space, and run models and projects both individually and collaboratively. Comparisons will be made with the running models as a guest tutorial to illustrate different use permissions.
The tutorial is written assuming you have already completed the running models as a guest tutorial, and have both a DOME server and client running locally on your computer. 
Step 1: Logging in as a user

.. image:: _static/as-user/1_guestBrowser.gif

Figure 1: View of run browser logged into your local server as guest.
If you just completed the running models as a guest tutorial, you will see the run browser as shown in figure 1. If not, log in as guest and expand items in the browser as shown in the figure.

.. image:: _static/as-user/2_userLogin.gif

Figure 2: Logging is as a tutorial user.

Next, press the run browser login button and the login window will appear. The top half of the login window has a combination box of your previous logins (provided the remember password box was checked when you logged in). This is convenient way to relog into a server if you are closing and opening new run browsers, or are switching between program modes. This information is destroyed when the DOME client application is quit.
You will now login as a user. The user tutorialUser has login permissions on your server, so enter this in the user name field. The password is 123. Since you are again logging into the local server, you can leave the server location field blank. Hit the lower login button (highlighted with the red box) to complete the login.

.. image:: _static/as-user/3_userSpace.gif

Figure 3: Logged in to empty user file space.

You can tell that you are logged in to the server as tutorialUser by looking at the text right below the server location combination box in the upper left of the window of figure 3.
When logged in as a user, the run browser is automatically set to show the user file space. Unless your server has been pre configured, there will be no users (including tutorialUser) with permission to to have their own space on the server. In Step 2 you will navigate to the general server file space where the tutorial models are located.

Step 2: Running models for individual use
In this step you will run several instances of the Epoxy/Carbon lamina model. Logged in as tutorialUser, you will have different access privileges from when you used the model logged in as guest.

.. image:: _static/as-user/4_serverCombo.gif

Figure 4: Changing the run browser to the server file space.

Switch the browser to the server file space as shown in figure 4. Note that the view combination box indicates that you are viewing models available for individual use that is separate, dedicated instances models will be created for you each time you open a model interface.

.. image:: _static/as-user/5_carbonLaminaInterfaces.gif

Figure 5: Epoxy/Carbon lamina model expanded to show two interfaces.

Once you have expanded the public folder, the tutorial folder, and the Epoxy/Carbon lamina model you will see two interfaces (figure 5). Any model or project can have many interfaces that provide different views for different users. In this case there are two interfaces that tutorialUser is allowed to see. 
If you press the back arrow (highlighted with the red circle in figure 5) the run browser will switch back to the guest login from which you started this tutorial. If you then expand the Epoxy/Carbon lamina model you will see that only one interface (lamina properties interface) is was made available for quests when the model was published using the deploy mode application.
If you are now looking at the window for the guest login, return to the tutorialUser login (as pictured in figure 5) by pressing the forward arrow to the right of the highlighted back arrow. In general, you may log into many different servers, or as different users on the same server, within a single run browser. The forward/back arrows can then be used to navigate between servers. You may also use the server location combination box at the top of the window for the same purpose.
Now, double click on the fiberInterface icon to open the interface. Alternatively, you may single click on the icon and then select open from the browser menu.

.. image:: _static/as-user/6_runningFiberInterface.gif

Figure 6: Running the fiberInterface to the Epoxy/Carbon lamina model.

The fiberInterface will open shortly and you can make and submit changes to run the model as shown in figure 6. The fiberInterface only exposes fiber related inputs and provides the properties of the lamina as outputs.

.. image:: _static/as-user/7_documentation.gif

Figure 7: Documentation for the fiber volume fraction vf.

Double click on the icon for the vf parameter.The GUI for the parameter (in this case a real number) will pop up in a separate window. Other data types, such as matrices or vectors have different GUIs. 
If you click on the documentation tab (figure 7) you will see that simple documentation has been provided for the parameter. All model interfaces and parameters support documentation.
Close the vf parameter by clicking on the X in GUI window title bar.

.. image:: _static/as-user/8_openLaminaInterface.gif

Figure 8: Opening the lamina properties interface. The fiberInterface is positioned underneath the run browser window.

The next step is to open a second interface to the Epoxy/Carbon lamina model. First move the fiberInterface window underneath the run browser window, as shown in figure 8. Click on the lamina properties interface and select open from the browser menu, as shown in the top of figure 8. 

.. image:: _static/as-user/9_laminaAndFiber.gif

Figure 9: The lamina properties interface and the fiberInterface, each connected to a separate instance of the model.

After a short period the lamina properties interface will open as shown in figure 9. First, note that the fiberInterface only exposes the subset of input parameters related to the fiber properties, whereas the lamina properties interface provides both fiber and matrix input parameters. 
Make and submit changes in the lamina properties interface and note that this does not affect the fiberInterface. If you make change in the fiberInterface this does not affect the lamina properties interface. 
This is because you are running interfaces to models available for individual use (indicated by the view combination box in the run browser). Each time a model interface is opened, a separate, dedicated instance of the model is created and connected to the newly opened interface. If you like, you can open a second independent version of the lamina properties interface, or you could use the back button on the run browser to open a third instance of the lamina properties interface logged in as guest.

.. image:: _static/as-user/10_selectFiberInterface.gif

Figure 10: Bringing the fiberInterface into the foreground using the Windows menu.

There are now several windows open. Use the Windows menu to bring the fiberInterface into the foreground.

.. image:: _static/as-user/11_closeFiberInterface.gif

Figure 11: Closing the fiberInterface.

With the fiberInterface in the foreground, select close from the Interface menu to close the interface and terminate the model for this interface on the server (see figure 11).

.. image:: _static/as-user/12_remaingLaminaInterface.gif

Figure 12: Running the lamina properties interface.

You will still have a running version of the lamina properties interface. You can make and submit changes, as in figure 12, since this instance of the model was not affected by closing the fiberInterface.
Close the lamina properties interface and any other interfaces to the Epoxy/Carbon lamina model that you may have opened.

Step 3: Running projects for individual use
In this step you will run integration projects, again for individual use. Your use permissions as tutorialUser will be different from when you ran the As cured lamina project while logged in as guest.

.. image:: _static/as-user/13_expandedProject.gif

Figure 13: Expand the As cured lamina properties project in your run browser.

Make sure that you are in the run browser window for tutorialUser. If you are looking at the guest window, use the forward button to navigate to the tutorialUser window. Expand the As cured lamina properties project in your run browser as shown in figure 13. 
In this case, the project was published using deploy mode with permissions that allows tutorialUser to see the interfaces of the contents of the project. You can see the overall project interface that you used in the running models logged in as guest tutorial in the interfaces folder. In the resources folder you can see the sub-models that are being integrated by the project. In the iModels folder you can see interfaces to the model inside of the project that joins the resource models.
If you wish, use the back button to switch to the guest browser window. Expand the project and verify that only the project interface is visible. This is because guests were not given privileges to view the contents of the project when it was deployed. Be sure to switch back to the tutorialUser window before continuing. 

.. image:: _static/as-user/14_openProject.gif

Figure 14: Opening the As cured lamina properties project.

You could select the project interface integrated model interface and open it to run the project with the same view as in the running models as guest tutorial. 
As an alternative, this time you will open the project GUI so that you will be able to make changes to the project interface and also see the changes propagate through the resource model interfaces.
Click on the As cured lamina properties project icon and then select open from the browser menu as shown in figure 14.

.. image:: _static/as-user/15_projectGui.gif

Figure 15: Making and submitting changes through the project GUI.

When the project GUI opens, expand the folders and models/interfaces until your view is the same as in figure 15. Change the lamina curing temperature and the nominal matrix modulus as shown in the figure and use the submit button to execute the models in the project. Note that the changes propagate through the interface of the Polymer curing model.
If you wish, you can also double click on the integrated model interface icon to open it in a separate window. Because the interface is being run from within the open project GUI, this interface window will be connected to the same running instance of the project.
When you are finished running the project, close the project window.

Step 4: Running models or projects in a collaboration playspace
Until now, all of the models and projects have been run for individual use, meaning that each open interface is connected to a different running instance of the model. In this step you will see how different clients can work together using a shared running instance of a model.

.. image:: _static/as-user/17_collaborationSwitch.gif

Figure 17: Switching to view collaborative use workspaces available in the server file space.

To begin, switch from the view from individual use model to collaborative use workspaces. Use the view combination box as shown in figure 17.

.. image:: _static/as-user/18_openPlayspace.gif

Figure 18: Opening the Polymer curing team playspace.

Expand the folders until your view is the same as in figure 18. In the server file space there is a collaboration playspace called Polymer curing team. Playspaces can be identified by the playspace icon to the left of the name.
Click on the playspace icon to select the playspace and then choose open from the browser menu as illustrated in figure 18.

.. image:: _static/as-user/19_runningPlayspace.gif

Figure 19: The open Polymer curing team playspace window.

After a short period a window will open for the polymer curing team playspace. A playspace may provide access to many models and projects. However, this playspace provides collaborative access to only one model, the Polymer curing model (see figure 19).
Expand the Polymer curing model until your window matches the view in figure 19. You can now make and submit changes directly in the playspace window. Make a change to the curing temperature and press the submit button. Then, change the value back to match figure 19.
Note: You can also double click on the polymer curing interface icon to open the interface in a separate window. If you choose to try this now, close the interface window before proceeding to the next step.

.. image:: _static/as-user/20_switchToGuest.gif

Figure 20: Navigating to the guest browser window and changing to the collaborative use view.

Normally different users collaborating through a playspace would be working on different computers. However, in this tutorial you will use the same computer to also enter the playspace as a guest user.
Begin by using the Windows menu to bring the run browser into the foreground. Figure 20 shows the run browser in the foreground with the open playspace (which you entered as tutorialUser) in the background. 
Use the back arrow (highlighted with a red circle) to switch to the guest browser window as shown in figure 20. Then use the view combination box to switch to collaborative use.

.. image:: _static/as-user/21_guestOpenPlayspace.gif

Figure 21: Opening the playspace logged in as the user guest.

Expand the folders to match the view in figure 21, select the playspace, and then open it.

.. image:: _static/as-user/22_twoPlayspaces.gif

Figure 22: Two playspace windows. The top playspace is for tutorialUser while the bottom user is for guest.

In a short moment a second playspace window will open. This playspace is for the user guest. Expand the Polymer curing model and its interface.
Move the new playspace window to the bottom of the screen and then click on the first playspace window (underneath the browser window ) to bring it in to the foreground. Your screen should look like figure 22.
You now have two playspace windows. The top playspace window is for tutorialUser. The bottom playspace window is for guest. This may be a bit confusing, but normally each user would be on a different computer.
You may now make and submit changes in either playspace window. The two playspace windows reflect the same values since guest and tutorialUser are sharing the same running instance of the model through the Polymer curing team playspace.
Note: Playspaces also have a conference feature that allows users in the playspace to chat with each other while running models. If you wish, press the conference button in each playspace window and pass messages back and forth between guest and tutorialUser. Close the conference windows before continuing to the next step.

.. image:: _static/as-user/23_closingGuestPlayspace.gif

Figure 23: Closing the playspace window.

When you are done, close one of the playspace windows by first bringing the window into the foreground and then choosing the close option from the Playspace menu.
Now, close the remaining playspace window.

.. image:: _static/as-user/24_closingRunBrowser.gif

Figure 24: Closing your run browser.

Finally, bring the run browser to the foreground and close it using the browser menu as shown in figure 24. Alternatively you can click on the X in the browser window title bar.