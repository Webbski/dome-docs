Running Published Examples
===========================
This tutorial is intended to familiarize you with how to run models and projects. After completing the tutorial you should be able navigate between DOME servers and execute simulations.

Tutorials Available: 

 * Running models as a guest. This tutorial covers using the DOME client to log into a server as a guest and run models and projects as an individual user.
 * Running models as a user. This tutorial covers using the DOME client to log into a server as a user and run models and projects, both individually and collaboratively. The tutorial is also used to illustrate different use privileges in comparison to the running models as guest tutorial.
 * Bookmarks. This tutorial covers using and adding bookmarks to interfaces, projects, and playspaces.

.. toctree::
   :titlesonly:

   as-guest.rst
   as-user.rst

