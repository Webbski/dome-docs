Running Models as a Guest 
==========================

In this tutorial you will log in to a DOME server as a guest, navigate the server file space, and run models and projects.
The tutorial is written assuming you have a DOME server running locally on your computer, and that you also have the DOME client application running.
Step 1: Logging in

.. image:: _static/as-guest/1_runMode.gif

Figure 1: Putting DOME into run mode.

Make sure that your DOME client is in run mode using the mode combination box, as shown in figure 1.

.. image:: _static/as-guest/2_newBrowser.gif

Figure 2: Opening a new run browser window.

Use the Run->New Window menu open a new run browser window, as shown in figure 2.

.. image:: _static/as-guest/3_runBrowser.gif

Figure 3: A new run browser window before logging in to a server.

A run browser, as seen in figure 3, will open. Run browsers are similar in concept to a web browser. There is no content in the browser because you have not logged into a server yet. 

.. image:: _static/as-guest/4_loginWindow.gif

Figure 4: Using the Login dialog to login as guest.

To login, press the login button to obtain the dialog as shown in figure 4. In this tutorial you will login as guest. Guest login does not require as password. 
The server field is used to specify which server you want to log into. This may be a server IP address or the server machine name. In this case you are logging into a server running on your computer, so you can use 'localhost' as the server address. You may type localhost or just leave the server field blank as shown in figure 4. By default this will log you in to the local server. When complete, hit the login button.
Note: An alternative way to login as guest without using the login dialog is to type the server location in the server combination box in the top left of the run browser and then press the 'guest button'. Again, a blank server location defaults to the localhost server.

.. image:: _static/as-guest/5_serverSpace.gif

Figure 5: Run browser logged in to your local server.

When the login is successful you will see a public folder in the server's file space. When you log in to a server as a guest, you are automatically placed in the server file space dedicated to running models individually. 
The combination box reading 'server' indicates that you are in the server file space--a general space managed by the administrator for all users. There may also be file spaces for specific users or groups on the server.
The view combination box indicates that you are in the part of the server file space that is dedicated to running models for individual use.

Step 2: Running models for individual use 
Running models for individual use means that a separate, dedicated instance of the model will be created for you when you start the model. This means that if other people are also working with the same model at the same time, changes they make will not affect your instance of the model and vice-versa. 

.. image:: _static/as-guest/6_tutorialExamples.gif

Figure 6: Models and projects available to guests in the server file space for individual use. 

Expand the Public and Tutorial Examples folders in the server file space (as shown in figure 6) by clicking on the triangles to the left of the folder icons. There are two models published in the Tutorial examples folder and one integration project that connects multiple models. 'Epoxy/Carbon lamina' and 'Polymer curing model' are models and 'As cured lamina properties' is a project, as indicated by their icons.

.. image:: _static/as-guest/7_laminaModel.gif

Figure 7: Interface to the Epoxy/Carbon lamina model

Next, expand the Epoxy/Carbon lamina model as shown in figure 7. A lamina is a sheet of carbon fibers woven into a fabric and then impregnated with an epoxy matrix. This material is used to make high-strength, light-weight components.
When you expand the model, inside you will see a single interface called lamina properties interface. Interface icons look like a book with a key hole. All models are run through interfaces that provide controlled views of parameters within the model. DOME automatically organizes the parameters (or variables) in the interface into an input/output structure. 
This interface allows the matrix (epoxy) tensile modulus and Poisson ratio to be changed as inputs, along with the fiber tensile modulus, Poisson ratio, and fraction of the total lamina. Properties of the lamina--principle moduli E1, E2; Poisson ratio nu12; shear modulus; and matrix volume fraction--are computed as outputs.
This view of the model and interfaces (in figure 7) is static view provided for the convenient browsing of models on a server. The next step is to start the interface so that the model can be executed.

.. image:: _static/as-guest/8_openInterface.gif

Figure 8: Opening the interface to start running the Epoxy/Carbon lamina model.

To open the interface and start running the model. first left click on the lamina properties interface icon. The name of the interface will become highlighted as shown in figure 8. Then go to the Browser menu and select open.

.. image:: _static/as-guest/9_openingModel.gif

Figure 9: Interface opening and executing the model to ensure that the interface and model are consistent.

In a short period the interface will pop up, as shown in figure 9. During this time a running instance of the model is created for you on the server. 
The interface's input values are sent to the model and the model executes to make itself consistent with the interface. A red background color means that the parameter has yet to be recomputed for the new input values. Green means that the value has been updated but other downstream values related to the change have yet to be computed. A white background means the values are fully up-to-date. When all of the background is white the model and interface are fully consistent.

.. image:: _static/as-guest/10_makingChanges.gif

Figure 10: Changing input values.

To change the value of the matrix tensile modulus, click on its cell in the value column (the cell background will turn blue), enter 700000, and hit the enter key. The background color of the cell will change to yellow as in figure 10. This means that the value is entered in your client, but it has yet to be submitted to the model running on the server. Note that the submit button in the bottom right of the interface GUI is now enabled.
Next, click on the value cell for the fiber tensile modulus and change it to 3.0E8. Then, instead of hitting enter after making the change, click on the value cell for the fiber volume fraction. The fiber volume fraction becomes editable and the fiber tensile modulus is automatically entered in your client. Change the fiber volume fraction to 0.4 as shown in figure 10.
Now, hit the submit button in the bottom right of the GUI. The values are sent to the model and it will execute based on the values that you have changed. You will see the background of outputs change from red to green to white. When all values are white the computation is finished.
At this point you may want to make changes and submit several times.
Note: If you start to change a value and would like to revert back to the previous value before entering it into the client, delete all characters in the value cell and hit return.

.. image:: _static/as-guest/11_buildView.gif

Figure 11: Changing from the Interface Causality View (input/output) to the Build View.

So far, you have been working with the Interface Causality View. This view is automatically generated by DOME. It structures the interface into inputs that can be changed by the user and outputs. It is also possible for the model builder to provide a different organization for the interface parameters. This is provided in the interface Build View (for builder's viewpoint). 
Figure 11 illustrates the process of using the view combination box to change the interface view. Left click on the combination box arrow on the right and select the 'Build View' option. In this case the builder organized the interface parameters along fiber, matrix, and lamina lines. 
You may also make and submit changes in build view. You will not be able to change the value of outputs. You may also switch back and forth between views at any time.

.. image:: _static/as-guest/12_closingModel.gif

Figure 12: Closing the interface

Once you have finished running the model, you may close the interface by selecting the close option in the interface menu. This is shown in figure 12. This will close your GUI and also terminate your instance of the model that is executing on the server. 
Note: If you do not see the interface menu it is because your interface GUI is not in the foreground. Click anywhere on the interface GUI and then the interface menu will become available. 

Step 3: Running an integration project for individual use 
Projects are a specific type of model that are used to connect any number of sub-models or projects together so that they operate as a single integrated simulation. 
Next, you will run a project that connects the lamina model with the polymer curing model. The polymer curing model computes the modulus for the epoxy matrix that accounts for the temperature at which it is cured. Before continuing, you may want to expand this model in your run browser to look at its interface.

.. image:: _static/as-guest/13_expandProject.gif

Figure 13: Interface causality view of As cured lamina properties integration project.

When you expand the As cured lamina properties project, you will see the integrated model interface shown in figure 13. The integrated system allows you to input the fiber tensile modulus, the lamina curing temperature, and the nominal matrix modulus.

.. image:: _static/as-guest/14_projectBuildView.gif

Figure 14: Selecting the build view of the project interface.

You can also see the build view of the project interface in the run browser before you open the project interface. First, select the project interface (integrated model interface) by clicking on the interface icon and then choose the Build option in the view menu, as shown in figure 14. The project builder has organized the parameters according to which model they are from.
You may start running the project by double clicking on the project's interface, or you may single click on the project interface icon and select open from the browser menu.

.. image:: _static/as-guest/15_openProject.gif

Figure 15: Running interface for the integrated project.

In a short period a running version of the project will appear as shown in figure 15. On the server, dedicated instances of the project and the lamina and processing model that it uses have been started. While starting, the project will solve to ensure that all of the models and the project interface are consistent.

.. image:: _static/as-guest/16_changingToGraph.gif

Figure 16: Entering changes and switching from list visualization to graph visualization of the interface.

Change the fiber tensile modulus and lamina curing temperature as shown in figure 16. These changes can now be submitted as you did when running the lamina model. However, in this case, before submitting the changes we will change from the spreadsheet-like list visualization to a graph visualization. This is done by clicking on the visualization combination box and choosing graph.

.. image:: _static/as-guest/17_runningGraph.gif

Figure 17: Project executing while in the graph visualization.

A graph visualization of the interface parameters is now shown. This view allows you to see how the different interface parameters are related to each other. You may submit your changes and watch the change propagate through the graph as the integration project executes. The image in figure 17 was captured during the execution process. You may use the visualization combination box to switch back to the list view to see parameter values at any time.
When you are finished making changes and submitting values, you may close the project by clicking on the X in the interface window title bar. The location of the X may be on the left or right depending on the operating system your computer uses (It is on the right for windows users). Alternatively, you may select the close option in the interface menu.
When you close the project, your running project and any other models that it uses are terminated on the server or servers. 