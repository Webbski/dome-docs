Integration Projects
===========================
This section of the tutorial is intended to familiarize you with how to build and deploy integration projects.
These tutorials assume that you have completed the running models tutorials and, at least, the rectangular solid with hole DOME model tutorial.
You will need access to a DOME server on which you have been provided with a user account with file space (so you can upload models to the server).

Tutorials Available:

1: Block and pin assembly: This tutorial covers the basics of building and deploying an integration project--creating a new project, subscribing to resource models, creating an integration model, defining project interfaces, deploying the project, and running the project.

2: Integrating two models to form an iterative loop: This tutorial covers building a DOME model with a loop, making interfaces, deploying the models and setting use permissions, and running the model.

.. toctree::
   :titlesonly:

   block-pin-assembly/index

