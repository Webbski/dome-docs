Block and Pin Assembly
===========================

This tutorial covers several aspects of building integration projects. 
You will make a project that keeps the block and pin consistent as if they were in an assembly, and you will also compute the assembly's cost.
You will subscribe to the pin and block models as shown on the right. The block is the same as was modeled in the block with hole tutorial.
Please check that you have covered the prerequisites for the tutorial as outlined on the main projects tutorial page. 

.. image:: _static/blockAndPin.gif

The tutorial is divided into the following sequential steps.

Step 1. Subscribing to models. You will use the build application to subscribe to the block and pin models.

Step 2. Integration: You will complete an iModel that contains relationships needed to join the two resource models.

Step 3. Adding project interfaces: You will create an interface for the model that defines a view that users will use to execute the model in run mode. 

Step 4. Deploying the project: You will deploy the project into your user account and set access permissions. 

Step 5. Running the project: You will execute the newly deployed model.


.. toctree::
   :titlesonly:

   step1.rst
   step2.rst
   step3.rst
   step4.rst
   step5.rst