Step 1: Making Subscriptions
=============================

In this step you will use build mode to start a new integration project. 
You will learn how to: navigate to locate published resource models (rectangular solid with hole and pin models) and add them to your project; add an integration model to your project (iModels are similar to DOME-models); and finally, how to add subscriptions to resource models to the iModel.
 
.. image:: _static/step1/Picture-1.gif

Figure 1: Opening a new project.

Working in build mode, select the Build-->New integration project menu item to open a new project, as shown in figure 1.

.. image:: _static/step1/Picture-2.gif
 
Figure 2: Changing the project name.

Change the project name to 'Block and pin assembly'. The name field is in the process of being edited in figure 2.
The next step will be to add resource models to the integration project. Resource models are published (deployed) sub-models that you are interested in working with--in this case the block (rectangular solid with hole) and pin models.
Adding resource models to a project simply makes the models accessible for subscription/use within the your project. If you are familiar with IDEs (integrated development environments), adding resources to a project is similar to making files known to a software project. The files can be part of your software project but you may not be using them in your code actively .

.. image:: _static/step1/Picture-3.gif
 
Figure 3: Initiating the process of adding resources to the project.

Start the process of adding the resource models by selecting the Add-->Resource menu item, as shown in figure 3.

.. image:: _static/step1/Picture-4.gif
 
Figure 4: Logging into the server where the resource models are located.

A resource browser will open, as shown in figure 4. You will use the resource browser like a run browser to locate the models or projects that you want to work with within your new project. 
Log into your local server using your username. Remember that joe smith is the tutorial placeholder for your own user name. If you have just completed the DOME model tutorials, you will probably have a cached login as shown in the figure.

.. image:: _static/step1/Picture-5.gif
 
Figure 5: Navigating to the Pin model and adding it to the project.

Once you have logged in, you will need to change from your use file space to the server file space, as highlighted in figure 5. Provided that your server installation was installed with the tutorial models, you will be able to navigate to the Tutorial resources folder shown in the figure. 
Click on the model icon (not the interface) to select the model and the press the add resource to project button.

.. image:: _static/step1/Picture-6.gif
 
Figure 6: Adding the resources model.

Move the browser window down slightly, as in figure 6, so that you can see the resource list in the project GUI. You will see the Pin model is located in the list. 
Select the Rectangular solid with hole model and once again press the add resource to project button. 
You should see the two resources in the project GUI, as highlighted in figure 6.
Press the done button to close the resource browser.

.. image:: _static/step1/Picture-7.gif
 
Figure 7: Renaming the resources.

Rename the Pin model with a new name more suited to this integration application, as is done in figure 7. This name is just a local name for convenience within the project. Note that the resource name is not lost and, of course, this has no effect on the actual deployed model. The green icon is the resource icon, and its name field is not editable--it always provides the actual name of the deployed resource model. 
Rename the 'rectangular solid with hole' model to 'Block for my assembly'.
Renaming allows you to provide meaningful model names for the context of your integration. For example, if you were modeling a car suspension, you might add a 'wheel' model as a resource to your project 4 times, and then give the 4 wheel models names left-front, left-rear, right-front, right-rear.

.. image:: _static/step1/Picture-8.gif
 
Figure 8: Adding an integration model to the project.

The renamed models can be seen in figure 8. 
The next step is to add an integration model to the project, in which you will define relationships to connect the block and pin models. Select the Add-->Integration model menu item as shown in figure 8.

.. image:: _static/step1/Picture-9.gif
 
Figure 9: Copying desired resource model interfaces into the clipboard.

The iModel is visible in figure 9. 
You are now ready to select the interfaces to which you wish to subscribe so they may be used in the iModel. In this case each resource model has only one interface, so select both models interfaces holding <ctrl><shift> for multiple selection. 
Copy the selected interfaces into the clipboard using the Edit-->copy menu item as shown in figure 9.

.. image:: _static/step1/Picture-10.gif
 
Figure 10: Adding subscriptions to the iModel.

Next double click on the iModel icon to open the iModel GUI. The iModel GUI is in the foreground in figure 10.
Use the Add-->Add and subscribe menu item to add interface subscriptions to the iModel.

 
.. image:: _static/step1/Picture-11.gif

Figure 11: Subscribed interfaces in the iModel.

In figure 11 you can see the subscribed interfaces in the iModel. The model name associated with each subscribed interface is in the value column. 
If you expand the pin interface, you can see its input/output parameters. Subscription parameters have yellow icons. You can also view the interface in its build view by selecting the interface and then choosing build from the View menu. However, for the next steps the interface causality view shown in the figure is convenient to work with.
You might note that the subscription interface looks very much like a procedural relation in a DOME model. In fact, you can think of the the subscribed interface simply as a special type of relation, and iModels are just a special type of DOME model that support the addition of subscribed interfaces. Therefore, the remainder of the modeling to integrate the pin and block models will be just like building a DOME model, as you did in the Rectangular solid with hole tutorial.

.. image:: _static/step1/Picture-12.gif
 
Figure 12: Changing the iModel name.

Change the iModel name to assembly integration model, as in figure 12. Also, expand both the pin interface and the volume and cost interface and take a moment to familiarize yourself with their contents.

.. image:: _static/step1/Picture-13.gif
 
Figure 13: Saving the project.

Save your work by choosing the Integration Project-->Save menu item. 
Give the project files the name blockAndPinAssembly, as in figure 13. A number of XML files will be saved to disk, so you may want to create a new folder for the project before saving.
You have now completed the process of adding resource models to the project and then adding subscriptions to a project iModel.