Using Custom GUI for Rectangular Solid Model 
==============================================

.. image:: _static/blockOnly.gif

All interfaces may have multiple custom GUIs in addition to the standard DOME GUIs seen thus far in the tutorial.
In this step you will use a custom interface GUI for the rectangular solid with hole model. This model is similar to the one you built in the DOME model tutorial but we will use a pre-deployed version of the model. 
We do assume you have completed the prerequisites for the DOME model tutorials.

.. image:: _static/usingCustomRect/Picture-1.gif

Figure 1: Log into your account.

Log into your user account on the server that you have been using for the tutorials. You will be placed in the individual use portion of your file space on the server, as in figure 1. joe smith is the tutorial placeholder for your user name.

.. image:: _static/usingCustomRect/Picture-2.gif
 
Figure 2: Opening the rectangular solid with hole model.

Switch to the server file space as shown in figure 2. Expand the folders until you can see inside of the Tutorial resources folder. Open the volume and cost interface of the Rectangular solid with hole model.

.. image:: _static/usingCustomRect/Picture-3.gif
 
Figure 3: The Standard DOME interface GUI.

As in figure 3, the interface will open and display the standard, Dome Default GUI. Use the GUI combination box (in the upper right of the window) to select the custom interface GUI. In general, a given interface may have none-to-many custom GUIs associated with it.

.. image:: _static/usingCustomRect/Picture-4.gif
 
Figure 4: The custom interface GUI.

The custom interface GUI is shown in figure 4. You may make changes and submit them as if working in the Dome Default GUI. In this case, the custom GUI has been written to provide a picture of the block.
You may switch back and forth between default and custom GUIs at any time.
This completes the using a custom GUI for the rectangular solid model tutorial.