Step 1: Associate the GUI with an Interface
============================================

In this step you will use build mode to associate a custom GUI with a model interface.
You will open an existing tutorial model, make a copy of it, associate a pre-defined custom GUI with one of its interfaces, and save your changes.

.. image:: _static/addingCustomRect/step1/Picture-1.gif

Figure 1: Opening an existing model.
Begin by using the Build-->Open model-->Dome Model menu as shown in figure 1.

.. image:: _static/addingCustomRect/step1/Picture-2.gif

Figure 2: Select the rectangularSolid model.

Navigate to the dome3/tutorialModels/interface/rectSolidCustomGUI folder. You will see the rectangularSolid model as shown in figure 2.

.. image:: _static/addingCustomRect/step1/Picture-3.gif

Figure 3: The opened model.

The model should open as in figure 3. The model's name is Rectangular solid with hole and custom GUI.

.. image:: _static/addingCustomRect/step1/Picture-4.gif

Figure 4: Save a copy of the model.

You should work with a different copy of the model, elsewhere on your file space, so that the models setup for the tutorial remain in tact. Use the Dome Model-->Save as... menu as in figure 4 for this purpose.

.. image:: _static/addingCustomRect/step1/Picture-5.gif

Figure 5: Naming your own copy of the model.

Once you have decided where to save the model copy, give it the name shown in figure 5.

.. image:: _static/addingCustomRect/step1/Picture-6.gif

Figure 6: Open the interface tool.

Now that you are working on a new copy of the model, open the interface tool using the Tools-->Interfaces menu as in figure 6.

.. image:: _static/addingCustomRect/step1/Picture-7.gif 

Figure 7: Opening the add custom GUI window.

Open the volume and cost interface. Then, use the graphical interface combination box to select the add... option as shown in figure 7.

.. image:: _static/addingCustomRect/step1/Picture-8.gif 

Figure 8: Giving the custom GUI a name.

Once the Custom GUI Chooser window has opened, complete the name to appear in interface combobox field as shown in figure 8.
Then press the select button to locate the pre-defined custom GUI that was made for the interface.

.. image:: _static/addingCustomRect/step1/Picture-9.gif 

Figure 9: Selecting the jar file contain the custom GUI.

Navigate to the dome3/tutorialModels/interface/rectSolidCustomGUI folder and choose the file rectVolumeAndCostGUI.jar.

.. image:: _static/addingCustomRect/step1/Picture-10.gif 

Figure 10: Set the main class in the custom GUI class.

The contents of the jar file will then be listed in the select main GUI class portion of the chooser, as shown in figure 10. This is the file that the interface will use to call the constructor for the custom GUI. Select the class file as in the figure, and then press the set button.

.. image:: _static/addingCustomRect/step1/Picture-11.gif 

Figure 11: Test the validity of the GUI class selection.

After you have selected the main class, press the test button to verify that the selected class is of the correct type, as shown in figure 11. Press OK to dismiss the dialog and the custom GUI chooser.

.. image:: _static/addingCustomRect/step1/Picture-12.gif 

Figure 12: Switching to the custom interface.

Now that you are back to the interface GUI, switch to the custom GUI using the graphical interface combination box, as in figure 12. 

.. image:: _static/addingCustomRect/step1/Picture-13.gif 

Figure 13: The custom interface GUI.

The custom interface GUI should appear as in figure 13. If the custom interface GUI is blank, check the message log for errors. Use the edit... option in the graphical interface combination box to check that you did not make any mistakes.

.. image:: _static/addingCustomRect/step1/Picture-14.gif 

Figure 14: Saving your changes.

Save your changes using the Dome Model-->Save menu item.The custom GUI is now associated with the volume and cost interface.