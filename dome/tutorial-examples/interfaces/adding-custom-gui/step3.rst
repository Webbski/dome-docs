Step 3: Running the Model 
============================

In this step you will use run the model and custom interface you deployed in step 2.
The process is the same as covered in the Using custom GUI for rectangular solid tutorial, with the exception that your model will be located in your own file space.
 
.. image:: _static/addingCustomRect/step3/Picture-1.gif

Figure 1: Opening the interface with a custom GUI.

Use a run browser to log into your account on the server. Expand your tutorial models folder, as shown in figure 1. Select the volume and cost interface and then open it.

.. image:: _static/addingCustomRect/step3/Picture-2.gif
 
Figure 2: Selecting the custom interface GUI.

The interface will open showing the default DOME interface GUI. Use the combination box, as in figure 2, to switch to the custom GUI. You may make and submit changes using the custom GUI or the Dome default GUI.