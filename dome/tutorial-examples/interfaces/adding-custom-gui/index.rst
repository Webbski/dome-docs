Adding Custom GUI to Rectangular Solid Model 
==============================================

.. image:: _static/blockOnly.gif

In this tutorial you will learn how to associate a custom interface GUI with a model interface. This tutorial is only needed if you anticipate writing custom user interfaces for the models that you build. 
You will open an existing model, associate a pre-defined custom GUI with the interface, deploy the model, and then run the model.

The tutorial is divided into the following steps. 

Step 1. Open model and associate a custom GUI: You will use the build application top open a model and then associate a custom GUI with one of its interfaces.

Step 2. Deploy the model and custom GUI: You will deploy the modified model.

Step 3. Using the custom GUI: You will use the custom GUI to execute the model in run mode.


.. toctree::
   :titlesonly:

   step1.rst
   step2.rst
   step3.rst
