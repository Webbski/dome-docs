Step 2: Wrapping the Excel Model
==================================
In this step you will use build mode to start a new Excel model and create a DOME wrapper model for the block with hole spreadsheet.
In this step you will: add parameters and map them to cells in the spreadsheet; define causality information for the model; and provide setup information so that DOME can find the excel spreadsheet.
 
.. image:: _static/step2/Picture-1.gif

Figure 1: Opening a new Excel model.

Begin by switching to the build mode application and use the Build-->New model-->Excel Model menu item, as in figure 1, to start a new Excel wrapper model. 

.. image:: _static/step2/Picture-2.gif

Figure 2: Adding a context to the model.

Instead of adding parameters directly to the model, you will start by adding a context to organize parameters in the build view. Use the Add-->Context menu item as shown in figure 2.

.. image:: _static/step2/Picture-3.gif
 
Figure 3: Renaming the context.

As shown in figure 3, give the context the name 'Independent parameters'. You will put the model inputs in this context.

.. image:: _static/step2/Picture-4.gif 

Figure 4: Two real parameters added to the context.

With the Independent parameters context expanded and selected, Use the Add-->Real menu item and add two parameters to the context. 
Rename the parameters as shown in figure 4. Set the values and units to match those expected in the Excel spreadsheet.

.. image:: _static/step2/Picture-5.gif 

Figure 5: Adding a second context to the model.

Now, add a second context to the model as shown in figure 5. Name the context 'Derived parameters'.

.. image:: _static/step2/Picture-6.gif 

Figure 6: Additional real parameters added to the model.

Using the Add-->Real menu item to add four additional parameters to the Derived parameters context. Name them and assign units as shown in figure 6.

.. image:: _static/step2/Picture-7.gif 

Figure 7: Adding cell references to map the parameters.

The next step is to provide information so that DOME can connect the model parameter values with the spreadsheet data. This mapping information has been added to the model in figure 7. 
With the spreadsheet open as a guide, enter the sheet and cell reference into the right column for all parameters in the wrapper model. For example, the hole diameter value is in Sheet1, cell B2--or using correct syntax Sheet1!B2.

.. image:: _static/step2/Picture-8.gif 

Figure 8: Defining the model causality.

Next, switch to the model causality tab and define the causality as shown in figure 8. Pressing the Edit causality information button opens the editor window.
This information is needed to that DOME can correctly solve systems of models. 

.. image:: _static/step2/Picture-9.gif 

Figure 9:Graph visualization of the model 

Once defined, you might want to switch back to the definition tab and switch to the Model Causality View or the graph visualization to verify the model causality, as shown in figure 9.

.. image:: _static/step2/Picture-10.gif 

Figure 10: Opening the model file list.

The next step is to associate the spreadsheet file with the wrapper model. 
Switch to the setup tab as shown in figure 10. Expand the files context and double click on the icon for the model files parameter. 

.. image:: _static/step2/Picture-11.gif 

Figure 11: Adding files to the model file list.

The model file parameter is a list. This list parameter will be used to define which Excel files are to be associated with the wrapper model. 
First, make sure the parmeter type combination box in the upper right of the model file window is set to file. This combination box is selected in figure 11. It should be set to file by default. Then press the add button to add a file parameter to the list.

.. image:: _static/step2/Picture-12.gif 

Figure 12: Editing the path of the file parameter.

Rename the new file parameter to match figure 12. Then, click in the value field and press the choose button to locate the Excel spreadsheet.

.. image:: _static/step2/Picture-13.gif 

Figure 13: Selecting the spreadsheet file.

Use the file chooser to navigate to and select the blockWithHole.xls spreadsheet file that is on your computer.

.. image:: _static/step2/Picture-14.gif 

Figure 14: File parameter with path to the excel spreadsheet.

The value of Block and hole spreadsheet paremeter is now the path to the spreadsheet file. Since there are no other files associated with the Excel model, close the model files window.

.. image:: _static/step2/Picture-15.gif 

Figure 15: Completing the remainder of the setup information.

The main model file is a parameter selected from the model file list. Since there is only one file parameter in this case, it is already set to the Block and hole spreadsheet parameter.
Moved files can be executed being true means that a copy of your excel spreadsheet file will be deployed to the server and executed within the server file space. Your local copy of the spreadsheet will not be used during model execution.
Make consistent when loaded is false. This means that the model will not automatically run when you open its interface. It will update correctly when there is a manual submission of values to the model.
The software version needs to be set to the version of Excel that is being used on the server where the model will be deployed. Change this to match your version. 
Run in foreground is false. This means that when users execute the wrapped Excel model on the server, Excel will run in the background.

.. image:: _static/step2/Picture-16.gif 

Figure 16: Renaming the model.

Switch back to the definition tab as shown in figure 16. Rename the model to 'Excel rectangular solid with hole'.

.. image:: _static/step2/Picture-17.gif 

Figure 17: Saving the model.

Use the Excel Model-->Save menu to open the file save dialog.

.. image:: _static/step2/Picture-18.gif 

Figure 18: Naming the model file

Give the model file the name shown in figure 18, and then save the model.
The wrapper model is now defined. The next step will be to create an interface for the wrapper model.