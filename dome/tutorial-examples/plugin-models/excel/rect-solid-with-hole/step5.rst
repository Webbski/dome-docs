Step 5: Running the Excel Model
==================================

In this step you will run the wrapped excel model that you deployed in step 4. You will be able to compare this model with the DOME-native version of the model.
In general, this material has been covered in the running published models tutorials so explanations in this tutorial are brief.

.. image:: _static/step5/Picture-1.gif

Figure 1: Run browser logged into your user account.

In run mode, log into your user account and navigate to your tutorial models, as shown in figure 1. joe smith is the tutorial placeholder for your user name. If you completed the DOME model tutorial number 1, you should see that both the Excel version of the rectangular solid with hole model and the DOME implemented version of the model are available.
Select the interface of the wrapped Excel model and open it by either double clicking on the icon, or by using the Browser-->open menu.

.. image:: _static/step5/Picture-2.gif

Figure 2: Interface to the Excel model open in build view.

You may now make changes and submit them to execute the model as in figure 2. 
Note the Excel version of the model is, from a user perspective, indistinguishable from the DOME implemented version (you may want to run the DOME version now for comparison). Thus, the Excel-based models can be used or subscribed to without any special distinction.

This completes the tutorial.