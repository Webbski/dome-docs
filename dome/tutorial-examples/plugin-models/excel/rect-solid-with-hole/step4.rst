Step 4: Deploying the Excel Model
====================================

In this step you will move the model and interface files that you saved in step 3 to a server where they can be executed by remote users. 
The deployment process is the same for any type of model, so it is covered briefly in this tutorial.
Remember, you need to deploy the Excel model to a server running the version of Excel that you chose when you completed figure 15 in step 2.

.. image:: _static/step4/Picture-1.gif 

Figure 1: Opening the deploy model wizard.

Switch to the Deploy mode application and use the Deploy-->Model menu item to open the model deployment wizard (figure 1).

.. image:: _static/step4/Picture-2.gif
 
Figure 2: Log into the server with your user account.

Log into the server that you have been using for the tutorials, as in figure 2. joe smith is the tutorial placeholder for your username.

.. image:: _static/step4/Picture-3.gif 

Figure 3: Select the Excel Wrapper model.

Proceed to the Select model stage of the wizard. Browse to select the model definition file as is shown in figure 3.

.. image:: _static/step4/Picture-4.gif 

Figure 4: Add a description for the model.

In figure 4 the description 'My first excel model' has been added as a description to appear with the model's name on the server.
Proceed to the next step.

.. image:: _static/step4/Picture-5.gif 

Figure 5: Selecting the deployment location on the server.

Select your tutorial models folder as the model's destination on the server, as has been done in figure 5. Proceed to the next step.

.. image:: _static/step4/Picture-6.gif 

Figure 6: Editing permissions.

Leave yourself with editing permissions as in figure 6. Editing permissions were outlined in figure 8 of step 4 in the DOME model version of the rectangular solid with hole tutorial. Proceed to the next step.

.. image:: _static/step4/Picture-7.gif 

Figure 7: Making the volume and cost interface available.

Check the volume and cost interface so that it will be available for use on the server as shown in figure 7. Proceed to the next step.

.. image:: _static/step4/Picture-8.gif 

Figure 8: Setting user permissions.

Leave yourself with use permissions for the interface, as is the case in figure 8. Use permissions were discussed in figure 10 of step 4 in the DOME model version of the rectangular solid with hole tutorial.
Proceed to the next step.

.. image:: _static/step4/Picture-9.gif 

Figure 9: Summary of deployment choices.

After reviewing the summary of your deployment options, as shown in figure 9, deploy the model to the server. The wrapper model definition file and a copy of the excel spreadsheet will be uploaded to the server. The assigned use permissions will be committed to the server database.
You are now ready to run the wrapped Excel model.