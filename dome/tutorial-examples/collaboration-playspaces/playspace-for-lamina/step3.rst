Step 3: Running Lamina Playspace
==================================


In this step you will use the playspace deployed in step 2.
This material has been covered partially in the running published models as a user tutorial. However, this step also illustrates using an interface GUI from within a playspace and the playspace chat tool (conference).

.. image:: _static/step3/Picture-1.gif

Figure 1: Logging into the server.

In run mode use a run browser window, as shown in figure 1, to log into the server with your account that you used in step 2 when deploying the playspace.

.. image:: _static/step3/Picture-2.gif
 
Figure 2: Switch to your collaborative use space.

When you log in, you will be placed in your individual use file space, as shown in figure 2. Use the view combination box to change to your collaborative use file space.

.. image:: _static/step3/Picture-3.gif
 
Figure 3: The lamina design team playspace.

When you expand your tutorial playspaces folder you will see the Lamina design team playspace, as shown in figure 3.
To enter the playspace, either double click on the playspace icon or select the playspace and choose the Browser-->Open menu item.

.. image:: _static/step3/Picture-4.gif
 
Figure 4: Running the Epoxy/Carbon lamina in the playspace GUI.

Within the open playspace window you can run models directly by expanding interfaces in tree view (figure 4).

.. image:: _static/step3/Picture-5.gif
 
Figure 5: Using an interface window within the playspace.

You may also open model or project interface windows from within the playspace. The lamina properties interface is shown in figure 5. Note that the value in the interface is the same as the value in the playspace window view. Both windows reflect the same running instance of the model. 
Further, if a model or project has multiple interfaces, you can open all interfaces and execute them on the same instance of the model. (In the individual use file space each interface would open with separate running instances of the model).

.. image:: _static/step3/Picture-6.gif
 
Figure 6: Conferencing with a collaborative user.

Of course, the main point of the collaborative playspace is to allow users in different places to work together on the same running instance of the models and projects within. 
The conference button (figure 6) has been used to open the playspace's chat tool. The tool lists all users that are members of the playspace, as well as those currently online and working in the playspace. 
On a remote machine the user tutorialUser has joined the playspace. Now, both you and tutorial user will be able to make changes and share the effect of each other's changes.
If you have a colleague who can log into your server and navigate to your user file space, you may want to try collaborating with them now.They are able to join because you gave guest permission to use the playspace (during step 2). The chat tool can be used to communicate with your collaborators while you are working with the models.
Close the playspace window. You have now completed the Lamina design team tutorial.