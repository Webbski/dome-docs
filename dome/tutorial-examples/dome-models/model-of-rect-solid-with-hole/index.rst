Model of a Rectangular Solid with a Hole
==========================================

This tutorial covers several aspects of building DOME models and the iModels that are used in projects to connect other models. Please check that you have covered the prerequisites for the tutorial as outlined on the main DOME models tutorial page. 
You will model the volume of a rectangle with a hole through its center. Later, the model will be revised to include a cost figure.
 
The tutorial is divided into the following sequential steps. 

Step 1. Starting a new model and building mathematical relations: You will use the build application to define relations for the rectangle geometry and volume.

Step 2. Connecting data in different relations: You will map parameters in different relations so that changes will propagate from one relation to another.

Step 3. Building interfaces for the model: You will create an interface for the model that defines a view that will be used to execute the model in run mode. 

Step 4. Deploying the model: You will deploy the model into your user account and set access permissions on the deployed model.

Step 5. Running the model: You will execute the newly deployed model.

Step 6. Revising the model: You will be introduced to DOME versions control and will add a relation to compute cost to the model

Step 7. Redeploying an updated version: You will redeploy the updated model as an updated version of the previously deployed model.

.. toctree::
   :titlesonly:

   step-1.rst
   step-2.rst
   step-3.rst
   step-4.rst
   step-5.rst
   step-6.rst
   step-7.rst