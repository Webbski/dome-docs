Step 5: Running the Model
=========================

In this step you will run the new model that you deployed in step 4.
This material has been covered in the running published models tutorials. If you chose to add the optional model view to the interface (in step 3) you will be able to try this view for the first time.
 
.. image:: _static/step5/Picture-1.gif

Figure 1: Log in to the server.

Begin by switching DOME to run mode and log into the server on which you have an account, as shown in figure 1. You will have your login cached from deploying the model in step 4.

.. image:: _static/step5/Picture-2.gif
 
Figure 2: Open the volume interface.

Navigate to your model as shown in figure 2. Select the volume interface and open it.

.. image:: _static/step5/Picture-3.gif
 
Figure 3: The running interface in Interface Causality View.

The model will open in Interface Causality View ( figure 3). This view organizes the parameters into inputs that the user can change and outputs. You may make changes and submit them to the model.

.. image:: _static/step5/Picture-4.gif
 
Figure 4: The interface build view

Switch the interface to Build View, as shown in figure 4. This is the view that you made when you created the interface (step 3). 
Run the model from this view. If you wish, you may also explore the different visualization options using the visualization combination box.
Option: If you added the model view in step 3, you can open the model view (using the model view button to the left of the message log). This view is read-only, but you can make changes in the main interface window and see data in the relations change as the model executes.
