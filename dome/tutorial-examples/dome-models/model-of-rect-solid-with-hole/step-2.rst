Step 2: Connecting Data
=========================

In this step you will connect data between the width/length relation and the volume relation created in step 1. 
You will learn how to: add mappings that connect the data in different parameters so that changes in the data of one object will propagate to the data of another object; interact with different model visualizations.
 

.. image:: _static/step2/Picture-1.gif

Figure 1: Adding a real parameter to a model.

To begin, the model should be open as it appears in figure 1. The first step will be to connect the holeDiameter in the width/length relation to the dia in the volume relation. 
With the current solving implementation, it is not possible to connect the input of one relation to the input of another relation directly. Instead, we will add a third parameter to the model (outside of the relations) and use this to synchronize the both variables holeDiameter and dia.
As a short cut, copy the relation parameter holeDiameter. Select the holeDiameter icon by clicking on it so it is highlighted as in figure 1. Then choose the Edit-->copy menu item. This action enters the selection into the clipboard.

.. image:: _static/step2/Picture-1.gif

Figure 2: Pasting a copy of the parameter into the model.

Now, make sure that neither of the two relations, or parameters within the relations (e.g. holeDiameter) are selected. If they are, you can deselect them by clicking on the white space at the bottom of the window. Nothing with the model will be highlighted, as in figure 2.
Use the Edit-->Paste copy-->Last selection menu item (also shown in figure 2) to paste a copy parameter of the parameter into the model. 
Note: If you are unfamiliar with the difference between pasting a copy of a parameter vs pasting a reference to a parameter, you may want to brush up on objects vs. references to objects and the edit menu options.

.. image:: _static/step2/Picture-1.gif
 
Figure 3: The copied parameter added to the model.

If you have been successful you will see the new model parameter called holeDiameterCopy at the bottom of the window, as shown in figure 3. Model parameters have red icons.
Note: If the parameter copy appeared inside one of the two relations, it means that the relation was selected when you pasted the copy. Use the Edit-->Delete menu item to delete the this parameter, and then click on white space at the bottom of the window to make sure that neither of the relations are selected. Then repeat the Paste-->Copy-->Last selection again.

.. image:: _static/step2/Picture-1.gif
 
Figure 4: Moving and renaming the pasted model parameter.

Move the new parameter to the top of the model (figure 4). Drag and drop is not supported yet, so select the new parameter by clicking on it, and then use the up arrow on the right of the window to move the parameter. The relations will collapse when you move the parameter, so you will need to re-expand them. 
Rename the model parameter as shown in figure 4.

.. image:: _static/step2/Picture-1.gif
 
Figure 5: Starting the connection (or mapping) process.

You will now start the process of connecting the diameter parameter to the holeDiameter and dia relation parameters using a mapping. After doing so, all three parameters (diameter, holeDiameter, and dia) will reflect the same value at run-time.
Select diameter, and then copy it into the clipboard using the edit menu, as shown in figure 5.

.. image:: _static/step2/Picture-1.gif
 
Figure 6: Mapping to the hole Diameter.

Next, select the holeDiameter parameter and then use the Add-->Map-->Last selection to create a mapping between the copied parameter (diameter) and the holeDiameter).

.. image:: _static/step2/Picture-1.gif
 
Figure 7: Mapping between holeDiameter and diameter.

If you have been successful, the model will appear as shown in figure 7.
If you accidentally make a mapping to the wrong object, you can delete the mapping using the mapping tool, which can be opened by double clicking on the parameters mapping column cell.

.. image:: _static/step2/Picture-1.gif
 
Figure 8: Mapping the dia parameter.

Repeat the mapping process for the dia parameter. Since diameter is still the current selection in the clipboard, you do not need to recopy it. Select the dia parameter as shown in figure 7 and then add a mapping to the last selection.

.. image:: _static/step2/Picture-1.gif
 
Figure 9: Successfully mapped parameters representing the hole diameter.

You will now see the mapping as shown in figure 9. diameter is mapped to both holeDiameter and dia. 
Note: Both holeDiameter and dia could have been mapped to diameter at the same time by selecting both holeDiameter and dia before performing the add--->map action. <ctrl><shift> is used for multiple selection.

.. image:: _static/step2/Picture-1.gif
 
Figure 10: Repeating the mapping process for the block width.

Now repeat a similar mapping process for the block width. 
Since width is an output of the width/length relation and w is an input to the volume relation they can be mapping directly. Input--output mappings can be made directly, unlike input--input mappings. Output--output mappings are not allowed as this would over constrain the model.
Select the width parameter and copy it into the clipboard as shown in figure 10.

.. image:: _static/step2/Picture-1.gif
 
Figure 11: Mapping width to the w parameter.

Select the w parameter and then use the add menu, as shown in figure 11, to make the mapping between width and w.

.. image:: _static/step2/Picture-1.gif
 
Figure 12: Mapped width and w parameters.

The mapping will now appear as shown in figure 12. 
Note: The values of the two widths may still be different in the build view, but they will be synchronized during model execution. If they are different, you can re-run the width/length relation and w will be synchronized with width. (select the width/length relation and then choose DOME Model-->Test Relation).

.. image:: _static/step2/Picture-1.gif
 
Figure 13: The length and l parameter are now also mapped.

Finally, repeat the mapping process to connect the output length to the input l. The mappings will appear as shown in figure 13. All mappings needed for this model are now complete.

.. image:: _static/step2/Picture-1.gif
 
Figure 14: Accessing the mapping tool.

The mapping process that you followed in this step uses menu shortcuts for mapping. You can also perform all mappings using the mapping tool. Figure 14 shows the location of the mapping tool under the tool menu. The mapping tool can also be opening by clicking on a cell in the mapping column.

.. image:: _static/step2/Picture-1.gif
 
Figure 15: Model causality view

So far you have been working in the model build view. The is the view in which you can add objects to the model. However, it is not a good view to understand the causal structure of a model once you start to link together relations using mappings.
In figure 15, the model has been switched to a Model Causality View. This view uses filters to sort the objects by causality. It is useful to determine what variables are free, or independent, and what variables are intermediate or result parameters.

.. image:: _static/step2/Picture-1.gif

Figure 16: Graph visualization.

In addition to the spreadsheet-like list visualization of the model, there are other visualizations that help to elucidate the structure of a the model. In figure 16, the visualization combination box has been used to switch to a graph visualization.
The right mouse can be used to collapse and expand regions of the graph, but in general this visualization is effective for fairly small models.

.. image:: _static/step2/Picture-1.gif
 
Figure 17: DSM visualization

Finally, in figure 17, the visualization combination box has been used to show a matrix, or design structure matrix (dsm), style presentation of dependency information. This view is helpful in larger models.

.. image:: _static/step2/Picture-1.gif
 
Figure 18: Return to the List visualization and Model Causality View.

As a final step, put your model visualization back to list and the Model Causality View. Step 2 is now complete.