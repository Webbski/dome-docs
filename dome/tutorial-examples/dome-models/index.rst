DOME Models
===========================

This section of the tutorial is intended to familiarize you with how to build and deploy DOME-native models.
These tutorials assume that you are familiar with materials covered in the running models tutorials, and that you have access to a DOME server on which you have been provided with a user account with file space (so you can upload models to the server).
If you do not have such an account, you should create and account for yourself on a local server that you can run on your computer. You can do this be completing the first step of the server management as an administrator tutorial now.
Tutorials Available: 

 * Model of rectangular solid with hole. This tutorial covers building a simple DOME model, creating interfaces for the model, deploying the model and setting use permissions, running the model, and editing updating the deployed model.
 * Model with iterative loop. This tutorial covers building a DOME model with a loop, making interfaces, deploying the models and setting use permissions, and running the model.
 * DOME models containing file objects: This tutorial covers building a DOME model that uses parameters containing data in external files.

.. toctree::
   :titlesonly:

   model-of-rect-solid-with-hole/index
   model-with-iterative-loop.rst
