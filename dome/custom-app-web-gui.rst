Custom Application/Web GUIs
==============================

The GUI Configurator is an application that is used to subscribe to DOME services available on the WWSW and to define a custom GUI for these services.

One can create custom GUIs using combinations of standard DOME3 GUI components or by connecting DOME services to a variety of Java Bean GUI components. Once a custom GUI is defined it can be saved so that the GUI and underlying DOME simulation runs through a standard web page. One can also save the GUI so it runs the DOME simulation through a customized desktop application.

The configurator tool is designed so that individuals can create web-based or standalone-application GUIs for DOME simulations without knowledge of software programming languages. The building environment is graphical. It is possible to rapidly define many different GUIs tailored to the needs and viewpoints of different users for a given simulation.

This tool has been prototyped and will be available in an upcoming summer 2002 release.
