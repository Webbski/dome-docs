Documentation Menu
==============================

The format documentation menu, as shown below, is available when an object's documentation editor is visible. Formatting capabilities provided under the Format documentation menu are:

 * Standard java fonts.
 * Font styles: plain, bold, italic, and underline.
 * Custom font sizes.
 * Custom font colors.
 * Paragraph justification

Cut, copy and paste functions are available by first selecting text and then clicking the right mouse (<ctrl> click on a Mac) .
ToDo: add screen shot of documentation context menu