Build Help Menu
===============

The build mode help menu contains standard help menu items, along with a direct link to the build mode section of the documentation. The point and click option enables context sensitive help.

.. image:: _static/help-menu.png