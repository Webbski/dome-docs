Build Menu
===========
The Build Menu is used to create or open models, projects and playspaces that are in your file space. Checkout is used to retrieve a deployed version from a DOME server so that it can be edited and, if desired, redeployed as a version update of the previously deployed model.
The exit option quits the entire DOME application.

.. image:: _static/build-menu.png