Parameters Filter
==============================

The parameters filter searches the model scope in which it is applied and creates references to all parameter objects that it finds. The filter GUI displays its contents using the list visualization view of the model GUI in which it resides. 
The filter is useful for determining what unique parameters are in a model.
 