Context Filter
==============================

The context filter searches the model scope in which it is applied and creates references to all context objects that it finds. The filter GUI displays its contents using the list visualization view of the model GUI in which it resides. 
The filter is useful for determining what unique context are in a model.