Indeterminates Filter
==============================

The indeterminates filter searches the model scope in which it is applied and creates references to all indeterminate parameters that it finds. Indeterminate parameters are parameters that cannot be deemed as either independent or dependent (driven by the output of relations). For example, a number of parameters in a twin relation will be indeterminate unless one of the twinned parameters is driven by another relation. 
The filter is useful for determining what indeterminate variables are in a model. The filter GUI displays its contents using the list visualization view of the model GUI in which it resides. 