Relations Filter
==============================

The relations filter searches the model scope in which it is applied and creates references to all relations objects that it finds. The filter GUI displays its contents using the list visualization view of the model GUI in which it resides. 
The filter is useful for determining what unique relations are in a model.