Outputs Filter
==============================

The output filter searches the scope in which it is applied and creates references to all parameters that are results from actions within its scope. All outputs must be results or intermediate variables within the filter's scope, but that may actually be only intermediate parameters from a global viewpoint if they are used by a relation outside of its scope. 
The filter is useful for determining what variables will change as a result of changes in inputs. The filter GUI displays its contents using the list visualization view of the model GUI in which it resides. 