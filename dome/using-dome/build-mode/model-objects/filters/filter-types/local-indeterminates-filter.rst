Local Indeterminates Filter
==============================

The local indeterminates filter searches the scope in which it is applied and creates references to all parameters that are indeterminate from within its local scope. Thus, local indeterminate parameters cannot be deemed as either independent or dependent within the filter's scope, but they may actually be determinate from a global larger scope.
The filter is useful for determining what parameters do not have a fixed causality within the local scope. The filter GUI displays its contents using the list visualization view of the model GUI in which it resides. 
