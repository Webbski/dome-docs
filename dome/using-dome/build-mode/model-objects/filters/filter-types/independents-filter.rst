Independents Filter
==============================

The independents filter searches the model scope in which it is applied and creates references to all independent parameters that it finds. Independent parameters are those which are not mapped to the output of any relations. Thus, the value of their data may be changed directly by model users. 
The filter is useful for determining what independent variables are in a model. The filter GUI displays its contents using the list visualization view of the model GUI in which it resides. 