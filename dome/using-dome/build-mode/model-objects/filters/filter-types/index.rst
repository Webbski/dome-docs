Filter Types
==============

A number of predefined filter types are currently available. In future releases it will also be possible for users to make custom filters.

 * Parameters Filter
 * Relations Filter
 * Context Filter
 * Visualization Filter
 * Independents Filter
 * Indeterminates Filter 
 * Intermediates Filter
 * Results Filter
 * Subscriptions Filter
 * Input Filter
 * Output Filter
 * Local Interdeterminates Filter 


.. toctree::
   :titlesonly:
   
   context-filter.rst
   independents-filter.rst
   indeterminates-filter.rst
   intermediates-filter.rst
   inputs-filter.rst
   local-indeterminates-filter.rst
   outputs-filter.rst
   parameters-filter.rst
   relations-filter.rst
   results-filter.rst
   subscriptions-filter.rst
   visualizations-filter.rst