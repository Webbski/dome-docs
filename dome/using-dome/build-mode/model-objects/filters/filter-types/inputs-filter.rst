Inputs filter
===============

The input filter searches the scope in which it is applied and creates references to all parameters that can be legally changed by actions outside of this scope. All inputs must be independent within the filter's scope, but may actually be dependent parameters from a global viewpoint if they are driven by a relation outside of its scope. 
The filter is useful for determining what variables may be legally changed by entities outside of the scope to which the filter is applied. The filter GUI displays its contents using the list visualization view of the model GUI in which it resides. 