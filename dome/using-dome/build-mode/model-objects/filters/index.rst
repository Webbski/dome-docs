Building Filters
===================

Filters are used to automatically orgainze model objects and a predefined. For example, the causal and object type views in DOME models are generated using filters. Since filters automatically determine their contents, they typically do not allow the direct addition of objects when building a model. However, they often support functions such as copy, delete, or mapping.




.. toctree::
   :titlesonly:
   
   filter-types/index
   filter-gui.rst
