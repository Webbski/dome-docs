Object Methods and Operators
==============================

Object methods and mathematical operators are used to manipulate the values and other characteristics of objects in relations that allow users to write custom code in their body definition (such as the procedural relation, for example). Methods and operators are available for:

 * Boolean
 * Enumerated
 * File
 * Integer Number
 * Matrix
 * Real Number
 * String
 * Vector
