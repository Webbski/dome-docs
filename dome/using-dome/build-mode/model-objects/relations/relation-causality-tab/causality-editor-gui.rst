Causality Editor GUI
==============================

The internal causality editor displays a square matrix with a row and column for each relation parameter. Check boxes in the matrix are used to indicate dependency relationships. The causality defintion does not automatically mirror the structure of the relation body. Care must be taken to ensure that the body of the relation and the relation causality definition are consistent. DOME needs this imformation to correctly solve sets of relations, so care must be taken.
The matrix is read in rows. As one moves accross a row, one should put checks in the columns that correspond to parameters that the row variable depends upond.
The example below assumes that the relation has interface parameters a, b, c, d, e, and that the relation body definition equations are:

c = a + b
e = c + d

The causality editor matrix should be completed as shown below. One can read that c depends upon a and b by reading across the row labeled c, and e depends upon c and d by reading across the row labeled e.

.. image:: _static/causalityEditor.gif

The causality editor is modal, meaning that editor must be closed using either the Ok or cancel buttons before other model windows may be selected.