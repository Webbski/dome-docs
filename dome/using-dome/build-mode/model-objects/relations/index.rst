Relations
============

Relations are used to define interactions or dependencies between parameters. There are a number of relations specialized for different purposes.
Relations are only supported in DOME Models and integration models (iModels).
All relations have parameters (relation interface parameters) upon which the relation performs its computation. When model parameters (or parameters from other relations) are mapped to a relation's parameters, changes within a relation will propagate to the mapped parameters outside of the relation.
Relations support documentation and constraints. Additionally, relations automatically perform dimensional analysis on computation involving data types that support units. The relation GUI section describes how to access relation definition functions in build mode.

.. image:: _static/proceduralRelation.gif


.. toctree::
   :titlesonly:

   relation-body-editor/index
   relation-causality-tab/index
   relation-interface-parameter-tab/index
   relation-types/index
   constraints.rst
   dimensional-analysis.rst
   examples.rst
   mapping-relations.rst
   relations-gui.rst
   relations-menus.rst
   testing-relations.rst
