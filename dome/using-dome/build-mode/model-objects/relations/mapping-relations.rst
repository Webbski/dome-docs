Mapping Relations
==============================

Relation interface parameters must be mapped to model parameters if the relation's execution is to have external effects. The mapping process is described in the DOME Model mapping parameters section. 