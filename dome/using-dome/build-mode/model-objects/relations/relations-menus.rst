Relation Menus
==================

 Each type of relation has its own context dependent add and edit menus for adding and editing relation interface parameters. Please see the relation-type specific documentation for details. 
