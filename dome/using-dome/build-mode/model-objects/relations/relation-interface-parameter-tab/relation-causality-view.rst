Relation Causality View
==============================

The input/output/local indeterminate view of relation parameters contains up to three filters. This view is useful for reviewing the internal causal structure of a relation independent of how the relation may be mapped in the model. 

 * The input filter shows interface parameters that are independent within the scope of the relation. Inputs may be changed by external parameters.
 * The output filter shows interface parameters that are computed (intermediate variables or results) by the relation as a function of its inputs. Outputs may be used but not changed by external parameters.
 * The local indeterminate filter shows interface parameters that do not have fixed causality based upon the internal structure of the relation. That is, interface parameters which have the potential to be both inputs and outputs are placed in this filter.

Since relations are specialized for different purposes, not all relations will have all three filters. Only relevant filters are included in the editor for each relation type. 
The example relation below is the same relation that is shown in the model causal view.

.. image:: _static/relationCausality.gif
