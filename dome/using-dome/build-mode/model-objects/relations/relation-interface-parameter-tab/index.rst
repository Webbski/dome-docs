Relation Interface Parameter Tab
=================================

The relation interface parameter tab is at the top of the relation GUI. An example for an equal relation is shown below. Relation interface parameter icons are blue. Relations perform their computation upon its interface parameters.
The parameter tab is used to set names, default values, and documentation, etc. for interface parameters. The relation parameters may also be edited in a model's list view. 
The view combination box on the right of the interface parameter editor toggles between two views: input/output/local indeterminate; and model causal. Relation parameters are automatically placed in these views depending upon the relation's internal causality and how relation parameters are mapped to other parts of the model. The up/down arrows on the right of the editor may be used to reorder interface parameters within filters.
Within a single relation, all interface parameters must have unique names. They have the same GUI as model parameters and can be configured in a similar manner. The values of these parameters will be used as a defaults in the relation if they are not mapped (or connected) to model parameters. 
Each relation type has an add and edit menu that is available when the relation GUI is in focus for adding and deleting interface parameters.

.. image:: _static/equalRelation.gif


.. toctree::
   :titlesonly:

   model-causality-view.rst
   relation-causality-view.rst
