Units Within Relation Body
==============================

Relation interface parameters can have units. Thus you may specify the units of interface parameters. Unit conversion and dimensional analysis is used to guarantee that all inputs enter the relation with the units specified, and leave the relation with the desired units and dimensions. 
However, during execution of the Python relation body (in relation types that support custom body definitions) the units of internal parameters may be converted to other dimensionally consistent units. 
Consider the following example. A procedural relation has the interface parameters...

TODO: tables

and the Python body of the relation is:

c = a + b 
d = c + 1.0

When the relation body executes, new internal real parameter objects are created in Python. The objects are assigned values and units as they were specified in the relation interface.
Then, the first line of the code is executed (c = a + b). The Real "+" operator will convert a and b into the same units (let's say cm), add their values, and return a Real object with a value of 5.0 and units in cm. Last, the = assigns the returned object to c.
Therefore, once the first line has executed, the internal c is 5.0 cm.
Next, the second line will be executed (d = c + 1.0). The Real "+" operator assumes that the primitive double data type has the same units as c, so a Real object is returned with a value of 6.0 and units in cm. Then, the = assigns the returned object to d. 
Therefore, after the second line has executed, the internal d is 6.0 cm.
Finally, the output interface parameters are updated.

 * 5.0 cm is converted to inches, and the interface parameter c is assigned 2.0 inches.
 * 6.0 cm is converted to inches, and the interface parameter d is assigned 2.4 inches.

The example shows that dimensional consistency of derived parameters is maintained within the relation, but the units used internally may vary from the units of the external interface parameters.


TODO: Tables

If, for some reason, you must know the units of derived internal relation parameters during relation execution, or you must combine primitive data types such as doubles or integers and DOME parameters with units, use the .copyFrom method described in the methods and operators section. Do not use the = assignment!
The same example is now repeated below using the Real parameter .copyFrom method instead of the = assignment.
A procedural relation has the interface parameters...

TODO: Tables

and the Python body of the relation is:
c.copyFrom(a + b) d.copyFrom(c + 1.0)
Once again, when the first line of the code is executed (c.copyFrom(a + b)), the Real "+" operator will convert a and b into the same units (cm), add their values, and return a Real object with a value of 5.0 and units in cm. However, unlike the = assignment (which assigned the returned object to c) the copyFrom method converts the result to 2.0 inches (from 5.0 cm), and sets the value of c to 2.0. Thus, the internal parameter c retains the units of inches specified in the relation parameter interface.
Thus, when the second line executes, c will be in inches, leading to the final output results being

 * The interface parameter c is 2.0 inches.
 * The interface parameter d is 3.0 inches.

TODO: Tables
