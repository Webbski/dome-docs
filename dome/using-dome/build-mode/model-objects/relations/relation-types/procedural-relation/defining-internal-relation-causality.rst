Defining Internal Relationm Causality
=============================================

Procedural relations require the users to define the causal relation between input and output interface parameters. Otherwise, all interface parameters will be seen as inputs. Care must be taken to ensure that the defined causality correctly matches the body definition.
The internal causality tab of the procedural relation editor GUI provides access to the causality editor.