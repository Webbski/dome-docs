Procedural Relation
====================

Procedural relations are so-named because you must write procedural code in the body of the relation. Procedural code is a recipe that provides instructions in the exact order needed to correctly solve interactions between input and output interface parameters. There are no external solvers. 
The procedural relation gathers values from its input interface parameters, executes its internal program, and once completed, updates the value of its output interface parameters. Procedural relations are the most flexible type of relationship because complete software programs can be written within the relation body. 
The procedural relation editor GUI supports the three steps required to fully define a procedural relationship: 

 * Adding interface parameters
 * Defining the relation's internal causality
 * Defining the relation body



.. toctree::
   :titlesonly:

   procedural-relation-menus/index
   adding-interface-parameters.rst
   adding-mappings.rst
   constraints.rst
   defining-internal-relation-causality.rst
   defining-the-relation-body.rst
   editor-gui.rst
   examples.rst
