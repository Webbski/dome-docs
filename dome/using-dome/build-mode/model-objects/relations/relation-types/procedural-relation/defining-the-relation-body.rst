Defining the Relation Body
==============================

The body definition panel of the procedural relation editor GUI provides access to the body editor.
Procedural relations require users to write the body code that relates input parameters to output parameters. Methods, operators and functions of parameter data types are used in the Python code relation body. Before writing relations it is recommended that you review the sections discussing units and data types within the relation body. 
Relation body definition samples are provided in the examples section.