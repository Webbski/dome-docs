Conversion and Mapping Compatibility
=============================================

Currently only identical data types may be mapped to each other, with the exception of integer and real parameters. This, a Real parameter may only be mapped to another Real or Integer parameter with the same dimension (units will be converted). In all other cases on identical parameter types may be mapped to each other. 
It is intended to implement conversions between many of the different data types in the future.
 
