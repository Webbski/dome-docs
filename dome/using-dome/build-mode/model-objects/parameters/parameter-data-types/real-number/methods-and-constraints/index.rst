Methods and Operators
======================

Real number methods and operators are used in Python expressions that manipulate parameters in relations. A brief summary is provided on this page and more detailed examples/explanations are available through the more details links. 
A Real number parameter is considered true if its value is non-zero, false if its value is zero. For other data type methods and operators see the method and operators page.

TODO: Tables

.. toctree::
   :titlesonly:

   real-methods.rst
   real-math-operators.rst
   real-math-functions.rst
   real-conversions.rst
   real-comparitors.rst
