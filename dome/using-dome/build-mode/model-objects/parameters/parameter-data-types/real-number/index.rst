Real Number
=============

The real number is a data type supports a single, floating-point value in double precision with units and constraints. Scientific notation is permitted. The data type is supported with methods and operators that may be used to manipulate its value in relations. 
The real number editor panel (shown below) appears in the parameter GUI definition tab. When you make a value change the background will show stale color until the change is committed. If you try to commit an invalid value, the background of the value field will maintain the stale color, indicating that the change has not been accepted.  The unit combination box on the right displays the unit assigned to the value. The change option in the combination box allows you to change the value's units. The constraint button is used to apply constraints to the data type.

.. image:: _static/real.gif

It is also possible to set the real value and unit using the value column in a model's list/tree-table view. The example below is in a DOME model. When you click on the value cell the editor, a value editing field and a unit combination box becomes available.

.. image:: _static/realList.gif

.. toctree::
   :titlesonly:

   methods-and-constraints/index
   real-constraints.rst

