String Methods and Operators
=============================

String methods and operators are used in Python expressions that manipulate parameters in relations. A brief summary is provided on this page and more detailed examples/explanations are available through the more details links. 
A String parameter is considered true if it is not empty, false if it is empty or null. For other data type methods and operators see the method and operators page.

TODO: Tables


.. toctree::
   :titlesonly:

   string-methods.rst
   string-operators.rst
   string-functions.rst
   string-conversions.rst
   string-comparitors.rst
