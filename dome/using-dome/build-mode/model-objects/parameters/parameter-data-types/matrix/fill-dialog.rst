Matrix Fill Dialog
=====================

The Matrix editor's fill button is used to open the fill dialog. The dialog is used to specific the initial value of new elements when rows or columns are added to the matrix. Additionally, if matrix elements are selected before opening the fill dialog, the fill value can be used to change the value of multiple existing elements in the matrix.


.. image:: _static/matrixFill.gif
