Matrix Add Dialog
===================

The Matrix editor's add button is used to open the add dialog. The dialog can be used to insert multiple rows or columns into the matrix in a specific location. Initial values for the row and column elements may also be specified. The default initial value is specified by the the value set in the fill dialog.


.. image:: _static/matrixAdd.gif