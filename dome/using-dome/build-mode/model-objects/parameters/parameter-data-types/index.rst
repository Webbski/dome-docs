Parameter Data Types
======================

Data types supported within parameters include those listed in the table below. A parameter's data type is set using the data type chooser.

TODO: Add table


.. toctree::
   :titlesonly:

   boolean/index
   enumerated/index
   file/index
   integer-number/index
   list/index
   matrix/index
   real-number/index
   string/index
   table.rst
   text.rst
   vector/index
