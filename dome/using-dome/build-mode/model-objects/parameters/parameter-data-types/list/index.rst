List
======

The List data type is used to create lists of DOME parameters. The data type also supports a number of methods and functions that can be used to in relations. 
Lists are indexed starting at zero. 
The List data panel, which appears in the parameter GUI definition tab, is used to add or delete parameters in a list. The type selected in the combination box determines what type of parameter is added to the list. The constraint button opens the list constraint editor.

.. image:: _static/list.gif

It is also possible to perform all list editing from a model's tree/table view (a DOME model is shown below). Parameters may be added, deleted or removed from this view using functionality available in the edit menu. Additionally, parameter references may be added to a lists using this view.

.. image:: _static/listListView.gif

.. toctree::
   :titlesonly:

   methods-and-functions/index
   list-constraints.rst
