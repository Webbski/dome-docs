List Methods and Functions
============================

List methods and functions are used to write Python expressions that manipulate parameters in relation types supporting custom body definitions. A brief summary is provided on this page while more detailed explanations are available through the more details links. 
A List data type is considered true if it contains elements, false if it contains no elements. Elements are indexed starting from 0. 
For other data type methods and operators see the method and operators page.

Methods:
TODO: Tables

Functions:

Comparitors:




.. toctree::
   :titlesonly:

   
   list-methods.rst
   list-functions.rst
   list-comparitors.rst

