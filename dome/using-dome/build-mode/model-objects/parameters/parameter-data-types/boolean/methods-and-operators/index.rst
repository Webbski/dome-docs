Boolean Methods and Operators 
===============================

Boolean methods and operators are used to write Python expressions that manipulate parameters in relations that support custom body definitions. A brief summary is provided on this page while more detailed explanations are available through the more details links. 
A Boolean parameter is considered true if its value is true, false if its value is false. There are two predefined constants of type Boolean: true and false. 
For other data type methods and operators see the method and operators page.

Methods:

TODO: Add tables

Logical Operators:

Type Conversions:

Comparitors:




.. toctree::
   :titlesonly:

   boolean-methods.rst
   boolean-logical-operators.rst
   boolean-conversions.rst
   boolean-comparitors.rst

