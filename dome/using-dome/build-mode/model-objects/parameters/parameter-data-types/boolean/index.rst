Boolean
============

The Boolean data type is a logical type that can have the state of either true or false. Boolean data types support a number of methods and operators that can be used in relations. 

The Boolean editor panel, which appears in the parameter GUI definition tab, is used to set the data type's value. The combination box sets the value to true or false.

.. image:: _static/boolean.gif

It is also possible to set the value of the Boolean data type using the value column in a model's list/tree-table view. The example below is in a DOME model.

.. image:: _static/booleanListView.gif

.. toctree::
   :titlesonly:

   methods-and-operators/index
