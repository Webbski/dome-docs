File
======

The File data type contains a path to a file on the local machine. File data types support a number of methods and operators that can be used in relations. 
The file editor panel, which appears in the parameter GUI definition tab, is used to set the file path. The file type combination box sets a filter on the file chooser. Once you pick a file of a given type using the choose button, the combination box is forced to match the selected file's type and is disabled. 
If show file in browser is checked, the file will be opened in the browser application designated for the chosen file's type. File data types support constraints.

.. image:: _static/file.gif

It is also possible to set the file using the value column in a model's list/tree-table view. The example below is in a DOME model. When you click on the value cell the editor, a file choose... button becomes available.

.. image:: _static/fileList.gif

.. toctree::
   :titlesonly:

   methods-and-operators/index
   file-constraints.rst