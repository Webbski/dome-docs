File Methods and Operators
============================

File methods and operators are used to write Python expressions that manipulate parameters in relation types supporting custom body definitions. A brief summary is provided on this page while more detailed explanations are available through the more details links. 
A File parameter is considered true if its size is greater than zero, false if its size is zero of the file is null. For other data type methods and operators see the method and operators page.

Methods:
TODO: Add Tables

Functions:

Comparitors:


.. toctree::
   :titlesonly:

   file-methods.rst
   file-comparitors.rst
