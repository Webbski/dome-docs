Vector
========

The Vector data type supports primitive integer or real numbers with units. There are also methods and operators that may be used in relations. Vector elements are indexed from 0.
The vector editor panel (shown below), appears in the parameter GUI definition tab and is used to set vector size, units, and the value of elements. The row and column radio buttons determine whether the vector is a column or a row.
The number of elements may be edited directly in the elements field, or using the add dialog. The delete button removes selected elements. If the fix size checkbox is selected it is not possible to change the length of the vector.
Element values may be typed directly into the vector table or many elements may be selected and changed to a common value using the fill button. The type combination box in the upper right of the GUI controls whether vector elements are real numbers or integers. 
The unit combination box is used to assign a unit to the vector and the constraint button opens the constraint editor.

.. image:: _static/vector.gif

It is also possible to change the size and unit of a vector in a model's value column. The example below is in a DOME model. When you click on the value cell an editor with an element number editing field and a unit combination box become available.

.. image:: _static/vectorList.gif


.. toctree::
   :titlesonly:

   add-dialog.rst
   fill-dialog.rst
   methods-and-parameters/index
   vector-constraints.rst

