Vector Add Dialog
===================

The Vector editor's add button is used to open the add dialog. The dialog can be used to insert multiple elements into the vector in a specific location. Initial values for the elements may also be specified. The default initial value is specified by the the value set in the fill dialog.

.. image:: _static/vectorAdd.gif
