Vector Methods
==============================

The Vector data type supports a copyFrom method and a duplication method. The copyFrom method should be used (not the = operator) only if you require internal relation parameters to maintain types and maintain units matching the relation interface parameters.
A summary of other Vector data type methods, operators and functions are on the methods and operators page.

TOOD: Tables