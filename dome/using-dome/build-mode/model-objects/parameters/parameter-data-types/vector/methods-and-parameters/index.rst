Vector Methods and Operators
==============================

Vector methods and operators are used to write Python expressions that manipulate parameters in relations. A brief summary is provided on this page and more detailed examples/explanations are available through the more details links. 
A Vector parameter is considered true if it has a non-zero dimension, false if has no dimension or is null. For other data type methods and operators see the method and operators page.

TODO: Tables


.. toctree::
   :titlesonly:

   vector-methods.rst
   vector-operators.rst
   vector-functions.rst
   vector-conversions.rst
   vector-comparitors.rst
