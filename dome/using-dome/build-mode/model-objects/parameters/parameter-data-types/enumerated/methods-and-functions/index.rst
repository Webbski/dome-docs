Enumerated Methods and Functions
=================================

Enumerated methods and functions are used to write Python expressions that manipulate parameters in relation types supporting custom body definitions. A brief summary is provided on this page while more detailed explanations are available through the more details links. 
An Enumerated data type is considered true if it contains elements, false if it contains no elements. Elements are indexed starting from 0. 
For other data type methods and operators see the method and operators page.

Methods:

TODO: Add tables

Functions:

Comparitors:




.. toctree::
   :titlesonly:

   enumerated-methods.rst
   enumerated-functions.rst
   enumerated-comparitors.rst