Parameter GUI
===============

A parameter GUI is shown below with its definition tab selected. Parameter GUIs open in a standalone window. When the definition tab is selected the parameter's data type editor is available.

 * The name field is used to edit the parameter's name. Text field editing commands are available.
 * The combination box to the right of the parameter name indicates its data type. You can use this combination box to change the data type.
 * The main panel of the definition tab is that data-type editor, which varies for each data type. Within this area there will be a button that opens a constraint editor for the data type.
 * Single clicking on the constant box (in the lower right of the GUI) toggles the parameter's data type between constant or variable. 
 * The documentation tab is used to access the parameter's documentation editor.

.. image:: _static/enumerationConstant.gif
