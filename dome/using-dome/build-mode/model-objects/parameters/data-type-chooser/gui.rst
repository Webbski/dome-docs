Data Type Chooser GUI
=======================

The data type chooser GUI is used to pick a parameter's data type from a list of available options. To open the data type chooser use the data type combination box in the parameter GUI. 
When you change a parameter's data type, value information related to the old data type is lost unless there is a conversion between types.
The data type chooser maintains program focus (it is a modal dialog), so you will not be able to select other DOME windows until it has been dismissed.

.. image:: _static/typeChooser.gif
