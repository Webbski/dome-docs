Units
========
DOME will automatically convert units between related parameters to maintain unit consistency.
All parameter data types that support units have a unit combination box in their editor panel. By default all data types supporting units initially have no units. Units can be chosen be selecting the change unit... option in the units combination box, which will open the unit chooser.

.. image:: _static/unitChange.gif

Additionally, there are a number of unitless constants defined under the constant cateogry in the unit chooser. For example 1.0 with the unit pi is 3.14...
It is also possible for to define custom units.

.. toctree::
   :titlesonly:

   units-chooser-gui.rst
   user-defined-units.rst
