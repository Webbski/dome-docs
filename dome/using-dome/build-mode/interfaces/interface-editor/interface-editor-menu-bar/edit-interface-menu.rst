Edit Interface Menu
==============================

The Edit Inteface menu appears only when the definition tab of an interface is selected. Edit options are dependent upon the standard interface view selected. All edit functions will not always be enabled in the menu.
The menu options apply only to DOME objects. Text field edit commands are available in a pop up menu. 

TODO: Tables

.. image:: _static/clipboardPaste.gif
