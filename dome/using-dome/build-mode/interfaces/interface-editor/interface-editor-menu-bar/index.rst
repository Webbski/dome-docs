Interface Editor Menu Bar
============================

The Interface editor menu bar is available whenever an interface editor GUI is in focus and the interface definition tab is selected. This provides access to edit interface, add, and tool functions.
In the figure below interfaces are being added to a Dome Model. The interface editor menus are appended beside the Dome model menu.
ToDo: add screen shot of Edit interface


.. toctree::
   :titlesonly:
	
   add-menu.rst
   edit-interface-menu.rst
   tools-menu.rst
