Building Interface Before the Model
=============================================

It is possible to add interface parameters and then map the interface parameters to the model at a later point in time. This can allow you to define and deploy a dummy interface for other to work with while the model underneath the interface is being completed.
To add interface parameters

 * Selecting the interface definition tab. 
 * Make sure that either the build view or interface causality view is selected.
 * Select Add->desiredParameterType from the add menu.

When interface parameters are added in this way, they must be also be mapped to model parameters in a separate step if they are to affect parameters in the underlying model. 
If unmapped interface parameters are added in build view, they are assumed to be inputs and thus will appear in the input filter of the causality view. To add an unmapped output interface parameter, select the output filter in the interface causality view before using the add menu. 
Once interface parameters have been mapped to model parameters, the causality of the model will always determine whether interface parameters are inputs or outputs.
ToDo: screen shot of interface with add menu expanded and a parameter selected. 
 
