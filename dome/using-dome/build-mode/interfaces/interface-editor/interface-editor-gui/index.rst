Interface Editor GUI
============================

The interface GUI is shown below with its definition tab selected. When the definition tab is selected, the edit interface menu bar (for adding objects, editing, and mapping to model objects) is available.

 * The name field is used to edit the name of the interface. The interface name can also be edited from the interface manager.
 * The GUI combination box is used to assign an optional custom run-time GUI to the interface.
 * The left arrow (beside build view) is used to change scope in the interface visualization panel.
 * The text box (containing build view in the figure) shows the name of the scope at which the interface is being viewed. There are three standard interface views that organize the interface parameters in different ways. The interface is currently being shown at the scope of the entire build view. When in build view you can define the contents of the interface. There is also a fourth model view that can be used to directly expose parts of the model underlying the interface. 
 * The visualization combination box is set to list (tree-table). 
 * The up/down arrows on the right are used to re-order selected objects within a context.
 * The values of interface parameters outside of the model view are user editable at run-time, provided that the changes do not violate the causality of the model. The interface causality view can be used to easily observe whether interface parameters are inputs or outputs.
 * The documentation tab is used to access the interface's documentation editor. Parameters within an interface can import documentation from a mapped model parameter.

ToDo: add screen shot of interface editor GUI


.. toctree::
   :titlesonly:

   interface-visualizations/index
   standard-interface-views/index