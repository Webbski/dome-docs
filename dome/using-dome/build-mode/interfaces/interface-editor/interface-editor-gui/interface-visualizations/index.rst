Interface Visualizations
============================

The visualization combination box in the Interface GUI is used to display the interface contents using different visualizations. For example, the inteface might be shown as a list or as text-based xml. The visualization combination box is not used to associate custom GUIs with an interface.
Only list visualization is available at present.

.. toctree::
   :titlesonly:

   list-visualization.rst
