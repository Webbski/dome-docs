Interface List Visualization
==============================

The list visualization contains ... (this documentation will be available soon). 
The behavior of this view follows the standard DOME list/tree-table. Double clicking on a mapping field to open the relation mapper tool.
Also need to document the editing of parameter data type values in this view and adding relation interface parameters through this GUI. Point out value and unit editor (when available) shown. For full data type editing and constraint definition, must work through the parameter GUI and the relation GUI.