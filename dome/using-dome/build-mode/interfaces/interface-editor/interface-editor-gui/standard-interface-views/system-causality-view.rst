Interface System Causality View
=================================

The system causality view contains four filters: independents; intermediates; results; and indeterminates.
The view presents interface parameters from an overall or global model causality viewpoint, unlike the interface causality view, which limits its scope to the internal causality of the model wrapped by the interface. 
This view is useful for seeing what input interface parameters or locally indeterminate interface parameters are actually dependent parameters from a global system model viewpoint, which accounts for how other remote models may have been connected to the model though the interface.
ToDo: add screenshot