Interface Causality View
==============================

The Interface Causality View is a standard interface view that includes a number of filters to organize parameters according to the overall causal structure of the interface: inputs filter, outputs filter, and indeterminate filter.
The contents of this view are generated automatically according to the causality of the model underlying model. However, unlike the model causality view, one can add interface parameters to this view using the edit interface menu bar. Thus, one may directly declare an unmapped interface parameter as an output by placing it in the output filter. However, once the interface parameter is mapped to a parameter in the model the model's causality will define whether it is an input (independent model variable) or an output (model intermediate or result variable). 
ToDO: add Screen shot
