Standard Interface Views
============================

The definition tab of the interface editor GUI provides three standard ways to organize the contents of an interface: build view, interface causality view, and system causality view. Additionally, there is a model view which can be used to directly expose selected objects from the model in a non-editable fashion.
The button to the right of the text field displaying the current view is used to select between the different views. 
ToDo: add new screen shot showing the button to select view.

.. toctree::
   :titlesonly:

   build-view.rst
   interface-causality-view.rst
   model-view.rst
   system-causality-view.rst
