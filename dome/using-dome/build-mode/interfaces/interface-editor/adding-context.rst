Adding Interface Context
==========================

Context can be added to an interface in for the purpose of structuring the build view of interface parameters or the model view. Context are not mapped to objects in the model that underlies the interface.
To add a context to the interface build view...

 * Select the definition panel of the interface editor GUI.
 * While in build view or model view, select add-->context from the add menu.

ToDO: add screen shot