Interface Manager GUI
==============================

The interface manager GUI is shown below. It is possible to expand interfaces to view their contents in the interface manager, but individual interfaces can only be edited by opening an interface's editor GUI. The list view follows general build mode GUI behavior. The default interface is automatically generated for all models.
ToDO: add screen shot of interface managers, with at least one interface expanded.