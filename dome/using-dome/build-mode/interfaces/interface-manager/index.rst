Interface Manager
==================

An interface manager is associated with each model. The interface manager lists all interfaces associated with a model and allows the addition, duplication, or deletion of interfaces using the interfaces menu bar. A non-user editable default interface will always be available in the list. The contents of individual interfaces are edited by double clicking on icons in the interface manager GUI.
The interface manager is accessed through a model's tool menu. The figure below shows the tools menu for a Dome model.
ToDo: add screenshot (tools menu expanded)

.. toctree::
   :titlesonly:

   default-manager-gui.rst
   default-manager-menu-bar.rst