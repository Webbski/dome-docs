Build Mode
==========

Build mode is used to create or edit models, projects, or playspaces and set up tools that can be applied to models. 
To start building:
Make sure the DOME3 application is running. The DOME3 menu bar will be visible across the top of your screen (unless the application has been minimized). 
Using the combination box on the right of the menu bar, select the build option. The Build Menu appears in the DOME3 Menu bar.

.. image:: _static/build-menu.png

In order to avoid being confused when you start to build models or projects, it is important to understand the difference between an actual object and viewing a reference to an object. You may also want to review the general behavior of the build mode GUI environment. 



.. toctree::
   :titlesonly:

   build-menu-bar/index
   models/index
   model-objects/index
   interfaces/index
   integration-projects.rst
   playspaces.rst
   tools.rst
   documentation-editor/index
   checkout.rst
   general-gui-behavior/index
   build-examples.rst