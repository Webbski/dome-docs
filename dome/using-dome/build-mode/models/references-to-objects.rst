References to Objects
======================

One of the most important concepts to understand when building and editing models is the difference between objects and references to objects. 
DOME3 models are made of different types of objects that capture data or describe interactions between data. 
The figure (at right) shows a model containing the parameters A, B and C (left of figure). The user's view of the model is seen through a DOME GUI. Views of models are created through context. In this case there are two context that contain references to objects in the DOME model. Context always contain references to objects, not the actual objects in the model. Thus, one can organize the model visualization using so that the same object C can be viewed through references in more than one context.

.. image:: _static/modelReference.gif

Now, if the reference B is selected and removed using the build GUI, the actual object B still remains in the DOME model.

.. image:: _static/modelReference1.gif
 
Finally, if one of the references to C is selected in the GUI and the delete function is chosen, the object C in the model will be deleted along with all references to C in the GUI.

.. image:: _static/modelReference2.gif
