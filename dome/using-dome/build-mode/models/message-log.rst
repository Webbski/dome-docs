Message Log
==============

A single message log is associated with every model. It is usually opened by a message log button in the upper right of the model GUIs (see the GUI section of the specific model type you are working on). 
In build mode, the message log is used to provide information or notification of events that do not merit interrupting users with a pop-up dialog.

.. image:: _static/messageLogGUI.gif

TODO: Add color reference table
