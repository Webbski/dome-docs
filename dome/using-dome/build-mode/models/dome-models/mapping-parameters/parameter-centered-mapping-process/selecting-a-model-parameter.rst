Selecting Model Parameters
==============================

The mappings for combination box in the mapping tool GUI contains a list of all of the model's parameters. Any parameter can be selected from the list so that it may be mapped to selected relation interface parameters. In this case the parameter Nuf is being selected.


.. image:: _static/mappingSelectModelParameter.gif
