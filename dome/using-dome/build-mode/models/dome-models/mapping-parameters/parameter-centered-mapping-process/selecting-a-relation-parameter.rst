Selecting a Relation Parameter
==============================

In order to select a relation parameter for mapping use the copy command in the edit menu. This may also be done from either the relation edit menu or the model edit menu. I this case the relation parameter x is selected.

.. image:: _static/mappingSelectRelParameter.gif
