Selecting the Model
=====================

The first item in the mapping tool GUI model/relation/interface combination box is the model associated with the mapping tool. 
Select the model from the list so that selected model parameters may be mapped to selected relation interface parameters.

.. image:: _static/mappingSelectModel.gif
