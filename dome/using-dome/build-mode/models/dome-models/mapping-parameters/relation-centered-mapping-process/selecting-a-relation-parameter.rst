Selecting a Relation Parameter
===============================


The mappings for combination box in the mapping tool GUI contains a list of all parameters within the selected relation. Any relation parameter can be selected from the list so that it may be mapped to selected model parameters. In this case the relation parameter x is being chosen.

.. image:: _static/mappingSelRelParameter.gif

