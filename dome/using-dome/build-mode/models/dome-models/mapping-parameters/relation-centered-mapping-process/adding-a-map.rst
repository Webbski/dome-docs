Adding a Map
===============

Once a relation, relation parameter, and model parameters are selected, you can add a map by using one of the map commands in the edit mapping menu. It is often convenient to keep the clipboard viewer open so the list of model parameters available for mapping is always visible.

When a mapping is made, the mapped model parameter will be appended to the mapping list and the mappings column of the model. In this case the parameter Ef has been mapped to x.

.. image:: _static/mappingAddRelMap.gif
