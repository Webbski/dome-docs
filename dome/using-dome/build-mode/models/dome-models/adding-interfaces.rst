Adding Interfaces
===================

Interfaces are used to define what elements of the DOME model will be exposed for users when the model is deployed.
Interfaces are adding to a model using the interfaces tool , which is accessible through the tools menu.

.. image:: _static/addingInterfaces.gif
