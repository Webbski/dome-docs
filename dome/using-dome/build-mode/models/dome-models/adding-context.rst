Adding Context
===============

Context are used to organize models in build view. Details of how to work with context are in the building context section. 
To add context to a DOME model...

 * Make sure the model's add menu is available by selecting the list visualization build view in the definition tab.
 * Select Add->Context from the add menu.

A reference to the context will be inserted into the model list visualization. Its location is determined by what is selected when it is added. If you open the context GUI in a new window, the DOME model menu remains available for model building.
References to context may be removed or they may be deleted from the model using the DOME model edit menu.

.. image:: _static/addContext.gif