Standard DOME Model List Views
================================

The DOME model list view is the main model building and editing view. There are three different types of list views that organize the contents differently: build view, object type view, and model causal view. The standard view combination box is used to switch between the the views. 

All list views follow standard DOME tree table behavior. There are three columns in the DOME model list views. The left-most name column contains object icons and names. The middle value column displays a summary form of the object's value. Many object types support editing in the value column. Units may also be edited from the value column. The right-most mapping column lists other parameters that an object is mapped to, and is applicable to parameters only. Double clicking in a cell within the mapping column opens the mapping tool.

The standard view combination box and view controls are only function in the list view. They are disabled in when other model visualizations are in use. The list view controls are summarized below. 

 * The left arrow (beside the standard view combination box (reading build view) is used to change visualization scope.The current scope name is listed in the combination box. 
 * The up/down arrows on the right are used to re-order selected objects within a context.

.. image:: _static/listView.gif


.. toctree::
   :titlesonly:

   build-view.rst
   object-type-view.rst
   causal-view.rst



