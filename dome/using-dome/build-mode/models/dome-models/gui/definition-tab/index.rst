DOME Model Definition Tab
==========================

When the DOME model definition tab is selected the DOME model menu (for adding objects, editing, mapping and defining interfaces) is available within the build menu bar.

The visualization combination box controls the overall type of model view. It is set to the standard list (tree-table) view in the figure below. The list view is the primary model editing view. There are also a number of alternative ways to visualize the model, including graph, matrix (DSM), and XML code. 

.. image:: _static/definitionPanel.gif

.. toctree::
   :titlesonly:

   standard-list-views/index


