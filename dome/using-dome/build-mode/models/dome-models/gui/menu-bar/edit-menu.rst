DOME Model Edit Menu  
======================


The Edit definition menu appears when the definition tab of a DOME Model GUI is selected. Edit options are dependent upon the type of object that is selected. Thus all edit functions will not always be enabled in the menu.

.. image:: _static/editMenu.gif

The menu options apply only to DOME objects. Text field edit commands are available in a pop up menu. 

TODO: Add table here



.. image:: _static/domeModelMenu.gif

