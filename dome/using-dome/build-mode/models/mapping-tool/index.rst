Mapping Tool
=============

Each model has a mapping tool. Mappings are used to connect parameters in a DOME model to relations or parameters in a model to interfaces. The mapping GUI opens in a standalone window (accessible through the model's tool menu or by double clicking on a mapping field in the model's list visualization). 

.. image:: _static/toolMenu.gif

There are also menu shortcuts for creating new mappings described in the DOME model mapping parameters and interface mapping sections, but the mapping tool must be used to remove existing mappings.

.. toctree::
   :titlesonly:

   gui/index
   edit-mapping-menu.rst