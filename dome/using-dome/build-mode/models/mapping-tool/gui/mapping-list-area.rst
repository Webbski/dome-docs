Mapping List
=================

The mapping list shows all parameters mapped to the parameter selected in the mappings for combination box. Mapped parameters may be added or deleted from the list using the edit mapping menu.

.. image:: _static/mappingList.gif
