Model, Relation, Interface Selector
=============================================

The model/relation/interface combination box allows you to select the view point from which you want to make mappings. 
For example, if the model is chosen as shown in below, all model parameters will be listed in the mappings for combination box. If a relation is chosen, the relation's parameters will be listed in the mappings for combination box. If an interface is chosen, the interface's parameters will be listed in the mappings for combination box.

.. image:: _static/modelSelector.gif
