Clipboard
==========
The clipboard viewer is opened from the build mode windows menu.
The clipboard is a buffer that records all object copy selections (made using the model's edit menu). Each copy selection is placed in a numbered folder. 
You can choose to paste copies of clipboard selections into any model (using the model's edit menu), provided the model support all objects in the selection.
The delete button removes selected elements from the clipboard (not the model), while the empty button clears the clipboard.

.. image:: _static/clipboard.gif