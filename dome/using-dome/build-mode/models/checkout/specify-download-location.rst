Model Checkout: Specify Location 
==================================

After selecting the model on the server, you will be prompted to pick a location on your local computer, to which the checked out model file will be downloaded. Any extra files associated with the model (such as custom GUIs, 3rd party model files) will also be saved in this location. On the Mac OSX, you will need to enter a name for the model file.

.. image:: _static/saveLocation.gif
