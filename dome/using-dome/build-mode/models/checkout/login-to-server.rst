Model Checkout: Login to Server 
================================

You will be prompted to login onto the server where the model files that you want to checkout are deployed. The server may be located anywhere in the world, and you may checkout visible model files provided you can log into the server and model deployers have given you editing permissions on the model file.

.. image:: _static/checkoutLogin.gif

