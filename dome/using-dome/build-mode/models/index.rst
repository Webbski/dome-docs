Models
=======
All standalone models are created using the build mode application (starting from the build menu). This section provides instructions for building models in DOME: 

* DOME Models (a DOME-native model)
* Abaqus Models (a batch plugin model)
* Adams Models (a batch plugin model)
* CATIA (a plugin model)
* Excel Models (a plugin model)
* I-deals Models (a plugin model)
* Matlab Models (a plugin model)
* Mathematica Models (a plugin model)
* Name-value Batch Model (a batch plugin model)
* Nastran (a batch plugin model)
* Solidworks Models (a plugin model)
* Unigraphics Models (a plugin model)

Before building DOME-native models you should understand the meaning of an object reference. 
All models have an associated message log and documentation editor. A clipboard is also available for copying and pasting model objects between different models.

.. toctree::
   :titlesonly:

   references-to-objects.rst
   dome-models/index
   plugin-models/index
   message-log.rst
   clipboard.rst
   mapping-tool/index
   model-versioning.rst
   checkout/index
   model-visualizations/index
   examples.rst
