Model Versioning
=================

Each time a model is saved, a new version number is included in the model file. This version number may be seen in the xml view but is not editable by users. 
The internal version number is used by version control mechanisms for checking deployed models out from servers for editing and redeploying updated versions of the edited models back onto a server.