Matrix (DSM) Visualization
==============================

All models support a Design Structure Matrix (DSM) visualization that can be selected using the visualization combination box. This view is useful for identifying what parameters depend on what, and in determining the order in which parameter data will be updated when a model is executed in run mode. For comparison, the DSM below is from the same model shown in list view in the main model visualization page. 

The first figure below shows the DSM in an unsorted form. The first row may be read as areaR1 is calculated using widthR1 and height R2. The first column may be read as arealR1 is used in calculating areaR2. 
The model is shown at the bottom of the page after the sort button has been pressed. Sorting algorithms are used to create a lower triangular matrix. This indicates the order in which items will updated when the model is solved at run time. If the model cannot be lower triangularized there will be dependencies shown in red above the diagonal. If the sorted matrix has such entries it means that there is a causal loop in the model. Run-time solving currently does not support such structures but this will be addressed in the neat future.
Editing is not supported in this view. 

.. image:: _static/visualizationDSM1.gif

.. image:: _static/visualizationDSM2.gif