Text Field Editing
==============================

Edit commands are such as cut, copy and paste are available in all GUI text field elements. 
In order to access the pop up edit menu, place the cursor in the text field, select text as desired, and then click the right mouse (<ctrl> click on a Mac) .
ToDo: Screen shot of edit pop up menu.