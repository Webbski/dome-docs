.. DOME documentation master file, created by
   sphinx-quickstart on Thu Oct 15 02:44:35 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Digital Manufacturing Commons Developer Documentation
=====================================================

.. note::
    This documentation is a work in progress. Topics marked with a TODO stub-icon are placeholders that have not been written yet. You can track the status of these topics through our public documentation `issue tracker <https://bitbucket.org/osmkhn/dome-docs/issues?status=new&status=open>`_.

Welcome to the DMC Developer Documentation. Users looking for instructions should refer to the Project DMC Wiki instead.

Topics
------

.. toctree::
    :maxdepth: 2
    :titlesonly:
 
    dmc/index
    dome/index
    

Contribute
-----------

The documentation on this site is auto generated from `this <bitbucket.org/osmkhn/dome-docs>`_ repository on BitBucket. You can contribute by submitting pull requests to contribute content or post corrections on the issue tracker.