# README #

This repo is used to build documentation for DOME hosted at [dome.readthedocs.org](http://dome.readthedocs.org/). Any commits here will automatically trigger a documentation rebuild. If there are issues with the documentation please submit one on the Issue Tracker. If you'd like to contribute, please send a pull request and it will be reviewed and merged.

### Documentation Sources ###

* Project DMC Website
* digitalmfgcommons.atlassian.net
* HTML Help inside DOME Client

### How do I get set up? ###

TODO

### Contribution guidelines ###

TODO

### Disclaimer ###
This repo is a volunteer effort to build clean, concise and approachable documentation for the DOME project. Sources used have been listed above. Copyright for initial documentation rests with DMDII and the Project DMC team.