Deployment
===================

First, create a development machine either from your local machine or from a seed AWS image. Then choose to deploy a stack either from the DMC repos or from your own code base.

Topics
------

.. toctree::
    :titlesonly:

    deployment-process/index
    install-terra.rst
    amazon-creds.rst
    stack-machines/index