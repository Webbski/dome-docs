DMC REST Services
=================

.. contents:: :local:

Purpose
-------

Present the DMC Front End a REST interface to connect to the DMC DB.

Access & Testing
----------------

To test, open ``ipofrest:8080/rest/services/test`` and you should receive the following JSON:
 
::

  {  
    "id":111,
    "title":"Test title",
    "description":"Test Description",
    "owner":null,
    "releaseDate":null,
    "tags":null,
    "specifications":"/services/111/specifications",
    "featureImage":null,
    "currentStatus":null,
    "serviceType":null
  }

Machine Type
------------
:Machine Type:      m4.large
:AMI base image:      
:east:              ami-12663b7a – this is RHEL 7 (64)
:east:              ami-18bac672 with Tomcat, Java, and Git
:west:
 
Installed software:

* Java ver. 1.8 – openJDK runtime 1.8.0_65-b17, 64-bit server VM build 25.65_b01 mixed mode
* Tomcat ver. 7
* Git ver. latest (2.4.3)

Connected to
------------

* DMCFrontEnd
* DMCDomeServer

Required Environment Variables
------------------------------
* DomeDBDns

Ingress Rules
-------------


Egress Rules
------------


Ingress / Egress Testing Status
-------------------------------
