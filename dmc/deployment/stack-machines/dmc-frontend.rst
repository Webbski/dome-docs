DMC Front End
=============

.. contents:: :local:

Purpose
-------

Present the DMC customer facing website to users.

Access & Testing
----------------

Automatic Deployment
^^^^^^^^^^^^^^^^^^^^

===================    ==============================================================================================================
Code repository:        `https://bitbucket.org/DigitalMfgCommons/frontend.git <https://bitbucket.org/DigitalMfgCommons/frontend.git>`_
Build process:          Compile (see instructions below)
Build tools:            Bamboo
Built artifacts:        
Tests:
Test tools:             Bamboo
Hosted at:              AWS S3 bucket
Deployed by:            Terraform
Deployed at:            AWS
===================    ==============================================================================================================

Manual Deployment
^^^^^^^^^^^^^^^^^

===================    ==============================================================================================================
Code repository:        `https://bitbucket.org/DigitalMfgCommons/frontend.git <https://bitbucket.org/DigitalMfgCommons/frontend.git>`_
Build process:          Compile (see instructions below)
Build tools:            ``gulp build``
Built artifacts:        ``/dist``
Tests:
Test tools: 
Hosted at:              ``/dist``
Deployed by:            Terraform
Deployed at:            AWS
===================    ==============================================================================================================

To compile:

First, install ``nodejs``, ``npm``, ``gulp``, and ``bower``:
::

  $ yum install nodejs npm --enablerepo=epel
  $ npm install -g gulp bower

Then, in the FrontEnd repository, run:
::

  $ npm install
  $ bower install
  $ gulp build
  $ cp -r dist/* <WEBROOT>/www/.

Machine Type
------------
:Machine Type:      m4.large
:AMI base image:      
:east:
:west:
 
Installed software:

* Apache2 ver.  ?
* php5 ver. ?
* git ver. ?
 
Depends On
----------
* GITServer
* SolrServer
* DBBackEnd (perhaps not any longer)
* DMCRestServices
* Shibboleth
* Kerberos
* ActiveMQServer
* DMCDOMEServer

Dependents
----------
None. 

Ingress Rules
-------------
* Port 443, tcp, GIT
* Port 80, tcp, Shibboleth (and the Internet)
* Port 22, tcp, SSH, Bastion only.

Egress Rules
------------
* Assume same for now.

Ingress / Egress Testing Status
-------------------------------
* Not tested.

Required Environment Variables
------------------------------
* DomeServerDns
* ActiveMqDns
* RestServiceDns