DMC ActiveMQ
=============

.. contents:: :local:

Purpose
-------

Holds the results of DOME execution until the DMCFrontEnd is ready to receive/display them.

Access & Testing
----------------

To access the activeMq manager visit ``ip.of.machine:8161/admin/``
 
To test that activeMq is running:
::

  netstat -nl|grep 61616"


Machine Type
------------
:Machine Type:      m4.large
:AMI base image:      
:east:
:west:
 
Installed software:

* Apache ActiveMQ ver. 5.10
 
Depends On
----------
No other instances.

Dependents
----------
* ForgeFrontEnd 
* DOMESingleServer

Ingress Rules
-------------
Connections are from DOMEServer and ForgeFrontEnd

* Port 61616, TCP and UDP – port on which ActiveMQ listens on for DOME input
* Port 61613, TCP, for connecting to STOMP.
* Port 8161, TCP, for web access.
* Port 22, TCP, for SSH, Bastion only.
* Port 8, TCP for ???

Egress Rules
------------
* Assume same for now.

Ingress / Egress Testing Status
-------------------------------
* Not tested.

Required Vars
-------------
* activeMqRoot
* activeMqRootPass
* activeMqUser
* activeMqUserPass

These variables are set in a configuration file called ``jetty-realm.properties``.