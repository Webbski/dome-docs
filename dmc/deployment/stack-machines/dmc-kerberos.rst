DMC Kerberos
============

.. contents:: :local:

Purpose
-------

Access & Testing
----------------

Machine Type
------------
:Machine Type:      
:AMI base image:      
:east:              
:west:
 
Installed software:

* Git ver. latest (1.9.1)
 
Depends On
----------

None.

Dependents
----------

* Shibboleth
* ForgeFrontEnd

Ingress Rules
-------------

* Port 9418, tcp
* Port 443, tcp for ForgeFrontEnd
* Port 22, tcp, SSH, Bastion only.

Egress Rules
------------
* Assume same for now.

Ingress / Egress Testing Status
-------------------------------
* Not tested.

Required Environment Variables
------------------------------
None.