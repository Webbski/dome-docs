DMC Shibboleth
==============

.. contents:: :local:

Purpose
-------


Access & Testing
----------------


Machine Type
-------------


Depends On
-----------

* DBBackEnd
* Kerberos

Dependents
----------

* ForgeFrontEnd

Ingress Rules
-------------

* Port 8443, tcp
* Port 80, tcp, ForgeFrontEnd
* Port 22, tcp, SSH, Bastion only.

Egress Rules
------------
* Assume same for now.

Ingress / Egress Testing Status
-------------------------------
* Not tested.