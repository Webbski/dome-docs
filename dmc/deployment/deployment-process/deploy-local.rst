Deploying the Stack from a Local Code Base
==========================================

Create a Dev Machine that allows you to edit the code and deploy your changes.

This process demonstrates mutable infrastructure. The purpose of this method is to show how one can deploy and develop for the DMC.

The production infrastructure builds are automated using Bamboo and integrated into a code delivery pipeline. We will expose more of our build and deployment tools in the near future.

This demonstrates how one may deploy manually and create infrastructure.

1. Go into the dmcdeploy_dev folder:
::

  $ cd ~/dmcdeploy_dev  
 
2. Switch branches to the devmakina branch:
::

  $ git checkout devmakina    
 
3. Pull in dev repos as needed (here we demonstrate the front end repo):
::

  $ cd ~ 
  $ git clone https://bitbucket.org/DigitalMfgCommons/frontend.git    
 
4. Make your code changes

We will change the website footer to have a ":)"
::

  $ cd /home/ec2-user/frontend/app/templates/common/footer 
  $ nano footer-tpl.html  

* Change a Lorem Ipsun line to  :)
* Save and exit    
 
5. Rebuild the app

Install the build tools on your dev system:
::

  $ cd ~/dmcdeploy_dev  
  $ nano build_tools.sh  

* Each infrastructure component has a different build process
* Edit the build_tools.sh to only install the tools you need to save time and space

::

  $ ./build_tools.sh  
 
FRONT END BUILD PROCESS
 
In the FrontEnd repository, run the commands below.  
::

  cd ~/frontend

INITIAL TOOLING INSTALL (only to be done once)
::

  $ npm install
  $ bower install    # answer 'n' to question when installing

Make your code changes, if you haven't done so already.

TO BUILD
::

  $ gulp build  

The output of this process can be found in the ``dist`` folder
 
Commit your changes to the frontend locally:
::

  $ cd ~/frontend

Forcibly add the dist folder to the commit
::

  $ git add dist -f
  $ git add -A 
  $ git commit -m "your message"  
 
6. Deploy Infrastructure that reflects your changes
::

  $ cd ~/dmcdeploy_dev  

Will only deploy front end to prove it works
::

  $ nano front.tf  

* Remove the first and last line comments
* Remove the dependency line
* Remove the setting of rest env val  
 
7.  Start the git daemon on the dev machine
::

  $ cd ~
  $ git daemon --base-path=. --export-all --reuseaddr --informative-errors --verbose &  

You should see "Ready to rumble" if all went well

8. Edit terraform.tfvars

::

  $ cd dmcdeploy_dev/
  $ nano terraform.tfvars  

* Input your AWS credentials on the first two lines
* Upload a key you wish to deploy with to this dev machine
    
::

  (quick shortcut env | grep MYip -- will show you your public ip )
 
From the machine that has the keyfile you wish to use:
::

  $ scp -i  mykey.pem somefile.txt ec2-user@my.ec2.id.amazonaws.com:/home/ec2-user/keys

Verify the key is there
::

  $ ls ~/keys/en  

* Edit key_name to the name of the remote key you just uploaded (without the .pem extension)
* Edit the key_full_path to " ~/keys/key_name.pem"
* Edit the stackPrefix  
* To launch the alpha release    
    * edit release = "alpha"
* To launch latest nightly    
    * edit release = "hot"  
 
9. Deploy new infrastructure  

Comment out (Add ``/*`` to first line and ``*/`` to last line) dev.tf if needed as will not be deploying dev machine
::

  $ nano dev.tf

Run 'terraform plan' and confirm that you are actually deploying the infrastructure that you intend:
::

  $ terraform plan

To actually deploy the infrastructure, run:
::

  $ ./deployFromDev.sh   
 
10. To prove that you successfully launched the front end process, run:
::

  $ terraform show
 
From the listing, determine the public IP of the aws_instance.front process. Open that IP in any browser.  You should see the DMC user interface.    
 
11. To destroy the infrastructure 
::

  $ terraform destroy 

Once you have deployed your infrastructure further changes to your stack need only be built and committed to your dev machine. To deploy those changes you ssh into the stack and execute updateMe.sh. This will update the code-base without having to relaunch the infrastructure.
