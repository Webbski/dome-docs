Deploying the Stack from the DMC Repositories
=============================================

The stack that this process deploys reflects the data found in the Digital Manufacturing repositories found at:
https://bitbucket.org/DigitalMfgCommons/

The purpose of this is to demonstrate the deployment of the stack from our stable repositories. It focuses on only the infrastructure deployment segment of the software cycle. This process is not applicable if you wish to alter the code as it will ALWAYS deploy from the DMC repositories.  If you wish to make code changes and see them reflected in your deployment, see Deploying the Stack from a Local Code Base instead.
 
1. Pick dev or deploy branch (here we choose dev to demonstrate):
::

  $ cd dmcdeploy_dev/  

We deploy just the front end to prove it works.
 
2. Configure:
::

  $ nano front.tf

* remove the first and last line comments
* remove the dependency line: ``depends_on = ["aws_instance.rest"]``
* remove the setting of rest environment variable: ``echo 'export Restip=${aws_instance.rest.private_ip}' >> ~/.bashrc``

3. Edit terraform.tfvars

* Input your aws credentials on the first two lines
* Upload a key you wish to deploy with to this dev machine    

::
        
  $ scp -i  mykey.pem somefile.txt ec2-user@my.ec2.id.amazonaws.com:/home/ec2-user/keys

* Edit key_name to the name of the remote key you just uploaded ( no .pem)
* Edit the key_full_path to " ~/keys/key_name.pem"
* Edit the stackPrefix  
* To launch the alpha release:
    * change release = "alpha"
* To launch latest nightly: 
    * change release = "hot"
 
4. Create the stack from the official repos of the DMC
::

  $ terraform plan -out a

Confirm that terraform will create the infrastructure that you want, then run:
::

  $ terraform apply a
 
5. To prove that you successfully launched the front end process, run:
::

  $ terraform show

From the listing, determine the public IP of the aws_instance.front process. Open that IP in any browser.  You should see the DMC user interface.    
 
6. To destroy the infrastructure:
::

  $ terraform destroy 