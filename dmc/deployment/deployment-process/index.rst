Deployment Process
===================

First, create a development machine either from your local machine or from a seed AWS image. Then choose to deploy a stack either from the DMC repos or from your own code base.

Topics
------

.. toctree::
    :titlesonly:

    create-local.rst
    create-seed.rst
    deploy-repo.rst
    deploy-local.rst